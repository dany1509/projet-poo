/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controler;

import Modele.*;
import Vue.*;
import java.sql.*;



/**
 *
 * @author Dany
 */
public class Reporting {
    private Connexion co;
    
    public Reporting(Connexion c){
        co = c; 
    }
    
    public ResultSet reportingChambre(String info){
        ResultSet result = null;
       
       try{
           if(info.equals("nblits")){
            //Affiche le résultat de la requete
            Statement state = co.getConn().createStatement();
            result = state.executeQuery("SELECT * FROM chambre");
            ResultSetMetaData resultMeta = result.getMetaData();
           }
           if(info.equals("tauxoccupation")){
            //Affiche le résultat de la requete
            Statement state = co.getConn().createStatement();
            result = state.executeQuery("SELECT * FROM chambre JOIN hospitalisation WHERE chambre.code_service = hospitalisation.code_service"
                    + " AND chambre.no_chambre = hospitalisation.no_chambre");
            ResultSetMetaData resultMeta = result.getMetaData();
           }
            
        }catch (Exception e) {
            e.printStackTrace();
        }
       
       return result; 
    
    }
    
     public ResultSet DocteurAyantAuMoinsUn(){
        ResultSet result = null;
       
       try{
      
            //Affiche le résultat de la requete
            Statement state = co.getConn().createStatement();
            
            result = state.executeQuery("SELECT prenom, nom FROM employe WHERE employe.numero IN (SELECT no_docteur FROM soigne WHERE no_malade IN (SELECT numero FROM malade WHERE numero IN (SELECT no_malade FROM hospitalisation)))");
            ResultSetMetaData resultMeta = result.getMetaData();
            
        }catch (Exception e) {
            e.printStackTrace();
        }
       
       return result; 
    
    }
     
     public ResultSet DocteurPas(){
        ResultSet result = null;
       
       try{
      
            //Affiche le résultat de la requete
            Statement state = co.getConn().createStatement();
            
            result = state.executeQuery("SELECT prenom, nom FROM employe WHERE numero IN (SELECT numero FROM docteur) AND numero NOT IN (SELECT no_docteur FROM soigne WHERE no_malade IN (SELECT no_malade FROM hospitalisation))");
            ResultSetMetaData resultMeta = result.getMetaData();
            
        }catch (Exception e) {
            e.printStackTrace();
        }
       
       return result; 
    
    }
     
     public ResultSet salaireMoyen(){
        ResultSet result = null;
       
       try{
      
            //Affiche le résultat de la requete
            Statement state = co.getConn().createStatement();
            
            result = state.executeQuery("SELECT * FROM infirmier");
            ResultSetMetaData resultMeta = result.getMetaData();
            
        }catch (Exception e) {
            e.printStackTrace();
        }
       
       return result; 
    
    }
     
     public ResultSet mutuelle(){
        ResultSet result = null;
       
       try{
      
            //Affiche le résultat de la requete
            Statement state = co.getConn().createStatement();
            
            result = state.executeQuery("SELECT * FROM malade");
            ResultSetMetaData resultMeta = result.getMetaData();
            
        }catch (Exception e) {
            e.printStackTrace();
        }
       
       return result; 
    
    }
     
     public ResultSet infirmierNuit(){
        ResultSet result = null;
       
       try{
      
            //Affiche le résultat de la requete
            Statement state = co.getConn().createStatement();
            
            result = state.executeQuery("SELECT prenom, nom FROM employe WHERE numero IN (SELECT numero FROM infirmier WHERE rotation=\"NUIT\")");
            ResultSetMetaData resultMeta = result.getMetaData();
            
        }catch (Exception e) {
            e.printStackTrace();
        }
       
       return result; 
    
    }
     
     public ResultSet rapport(){
        ResultSet result = null;
       
       try{
      
            //Affiche le résultat de la requete
            Statement state = co.getConn().createStatement();
            
            result = state.executeQuery("SELECT nom, (SELECT COUNT(*) FROM INFIRMIER WHERE INFIRMIER.code_service = SERVICE.code)/(SELECT COUNT(*) FROM HOSPITALISATION WHERE HOSPITALISATION.code_service = SERVICE.code) AS rapport_i_sur_m FROM SERVICE");
            ResultSetMetaData resultMeta = result.getMetaData();
            
        }catch (Exception e) {
            e.printStackTrace();
        }
       
       return result; 
    
    }
     
    public ResultSet employeD(){
        ResultSet result = null;
       
       try{
      
            //Affiche le résultat de la requete
            Statement state = co.getConn().createStatement();
            
            result = state.executeQuery("SELECT specialite, (SELECT COUNT(*) FROM employe WHERE employe.numero IN (SELECT numero FROM docteur))/(SELECT COUNT(*) FROM employe) AS rapport_d_sur_e FROM docteur");
            ResultSetMetaData resultMeta = result.getMetaData();
            
        }catch (Exception e) {
            e.printStackTrace();
        }
       
       return result; 
    }
    
    public ResultSet employeI(){
        ResultSet result = null;
       
       try{
      
            //Affiche le résultat de la requete
            Statement state = co.getConn().createStatement();
            
            result = state.executeQuery("SELECT numero, (SELECT COUNT(*) FROM employe WHERE employe.numero IN (SELECT numero FROM infirmier))/(SELECT COUNT(*) FROM employe) AS rapport_i_sur_e FROM infirmier");
            ResultSetMetaData resultMeta = result.getMetaData();
            
        }catch (Exception e) {
            e.printStackTrace();
        }
       
       return result; 
    }
    
    public ResultSet repartitionS(){
        ResultSet result = null;
       
       try{
      
            //Affiche le résultat de la requete
            Statement state = co.getConn().createStatement();
            
            result = state.executeQuery("SELECT specialite FROM docteur");
            ResultSetMetaData resultMeta = result.getMetaData();
            
        }catch (Exception e) {
            e.printStackTrace();
        }
       
       return result; 
    }
    
    public ResultSet nbSoigne(){
        ResultSet result = null;
       
       try{
      
            //Affiche le résultat de la requete
            Statement state = co.getConn().createStatement();
            
            result = state.executeQuery("SELECT employe.numero, employe.prenom, employe.nom, COUNT(*) as nombremalades from employe, soigne WHERE employe.numero = soigne.no_docteur GROUP BY employe.nom, employe.prenom");
            ResultSetMetaData resultMeta = result.getMetaData();
            
        }catch (Exception e) {
            e.printStackTrace();
        }
       
       return result; 
    }
    
    public ResultSet hospitalisation(){
        ResultSet result = null;
       
       try{
      
            //Affiche le résultat de la requete
            Statement state = co.getConn().createStatement();
            
            result = state.executeQuery("SELECT hospitalisation.code_service, COUNT(*) as nombre_hospitalisations from hospitalisation GROUP BY hospitalisation.code_service");
            ResultSetMetaData resultMeta = result.getMetaData();
            
        }catch (Exception e) {
            e.printStackTrace();
        }
       
       return result; 
    }
    
    
     
     
     
     
}
    
