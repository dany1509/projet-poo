/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controler;

import Modele.*;
import Vue.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.awt.event.*;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;

/**
 *
 * @author Dany
 */
public class Recherche {

    private ResultSet resultat;

    //private InterfaceRecherche ir; 
    public Recherche() {

    }

    public ResultSet All(Connexion c, String table, String colonneSelect) {
        ResultSet result = null;
        try {
            Statement state = c.getConn().createStatement();
            result = state.executeQuery("SELECT " + colonneSelect + " FROM " + table + "");

            //On récupère les MetaData
            ResultSetMetaData resultMeta = result.getMetaData();

//            System.out.println("\n**********************************");
//            //On affiche le nom des colonnes
//            for(int i = 1; i <= resultMeta.getColumnCount(); i++)
//                System.out.print("\t" + resultMeta.getColumnName(i).toUpperCase() + "\t *");
//         
//            System.out.println("\n**********************************");
//         
//            while(result.next()){         
//                for(int i = 1; i <= resultMeta.getColumnCount(); i++)
//                    System.out.print("\t" + result.getObject(i).toString() + "\t |");
//            
//                System.out.println("\n---------------------------------");
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public ResultSet Requete(Connexion c, boolean RechercheAvancee, JCheckBox startWith, String table, String crit,
            String field, String table2, String table3, String crit2, String crit3, String colonneSelect) {

        ResultSet result = null;

        try {

            //Affiche le résultat de la requete
            Statement state = c.getConn().createStatement();
            System.out.println("RechercheAvance: "+RechercheAvancee);
            if (!RechercheAvancee) {
                if (startWith.isSelected()) {

                    result = state.executeQuery("SELECT " + colonneSelect + " FROM " + table + " WHERE " + crit + " LIKE '" + field + "%'");
                } else {
                    result = state.executeQuery("SELECT " + colonneSelect + " FROM " + table + " WHERE " + crit + "='" + field + "'");
                }
            } else if (RechercheAvancee && table3.equals("")) {
                if (!crit2.equals("") && field.equals("")) {
                    result = state.executeQuery("SELECT " + colonneSelect + " FROM " + table + " INNER JOIN " + table2
                            + " ON " + table + "." + crit + " = " + table2 + "." + crit2);
                } else if(crit2.equals("") && field.equals("")){
                    
                    result = state.executeQuery("SELECT " + colonneSelect + " FROM " + table + "," + table2 );
                }
                else if(!crit2.equals("") && !field.equals(""))
                {
                    System.out.println("OUI");
                    result = state.executeQuery("SELECT " + colonneSelect + " FROM " + table + "," + table2 
                    + " WHERE " + table + "." + crit + " = " + table2 + "." + crit2
                    + " AND " + table + "." + crit + " = '" + field + "'");
                }
            } else if (RechercheAvancee && !table3.equals("") && !table2.equals("") && !table.equals("")) {

                if (crit3.equals("") && !crit2.equals("") && field.equals("")) {
                    result = state.executeQuery("SELECT " + colonneSelect + " FROM " + table
                            + "," + table2 + "," + table3
                            + " WHERE " + table + "." + crit + " = " + table2 + "." + crit2);
                } else if (!crit3.equals("") && field.equals("")) {

                    result = state.executeQuery("SELECT " + colonneSelect + " FROM " + table + "," + table2 + "," + table3
                            + " WHERE " + table + "." + crit + " = " + table2 + "." + crit2
                            + " AND " + table2 + "." + crit2 + " = " + table3 + "." + crit3);
                }
                else if(crit3.equals("") && crit2.equals("") && crit.equals("") && field.equals("") )
                    result = state.executeQuery("SELECT " + colonneSelect + " FROM " + table + "," + table2 + "," + table3 );
                
                else if(!crit3.equals("") && !field.equals(""))
                    result = state.executeQuery("SELECT " + colonneSelect + " FROM " + table + "," + table2 + "," + table3
                    + " WHERE " + table + "." + crit + " = " + table2 + "." + crit2
                    + " AND " + table2 + "." + crit2 + " = " + table3 + "." + crit3
                    + " AND " + table + "." + crit + " = '" + field + "'");

            }
            
            if(result == null)
                JOptionPane.showMessageDialog(null, "Recherche invalide !", "Erreur", JOptionPane.ERROR_MESSAGE);


            //On récupère les MetaData
            ResultSetMetaData resultMeta = result.getMetaData();

//            System.out.println("\n**********************************");
//            //On affiche le nom des colonnes
//            for(int i = 1; i <= resultMeta.getColumnCount(); i++)
//                System.out.print("\t" + resultMeta.getColumnName(i).toUpperCase() + "\t *");
//         
//            System.out.println("\n**********************************");
//         
//            while(result.next()){         
//                for(int i = 1; i <= resultMeta.getColumnCount(); i++)
//                    System.out.print("\t" + result.getObject(i).toString() + "\t |");
//            
//                System.out.println("\n---------------------------------");
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //resultat = result; 
        return result;
    }

}
