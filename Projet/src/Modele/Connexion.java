/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Modele;
import java.util.*;


/*
 * 
 * Librairies importÃ©es
 */
import java.sql.*;
import java.util.ArrayList;
/**
 * 
 * Connexion a votre BDD locale ou Ã  distance sur le serveur de l'ECE via le tunnel SSH
 * 
 * @author segado
 */
public class Connexion {

    /**
     * Attributs prives : connexion JDBC, statement, ordre requete et resultat
     * requete
     */
    private Connection conn;
    private Statement stmt;
    private ResultSet rset;
    private ResultSetMetaData rsetMeta;
    /**
     * ArrayList public pour les tables
     */
    public ArrayList<String> tables = new ArrayList<>();
    /**
     * ArrayList public pour les requÃªtes de sÃ©lection
     */
    public ArrayList<String> requetes = new ArrayList<>();
    /**
     * ArrayList public pour les requÃªtes de MAJ
     */
    public ArrayList<String> requetesMaj = new ArrayList<>();
    
    public Connection getConn(){
        return conn;
    }

    /**
     * Constructeur avec 3 paramÃ¨tres : nom, login et password de la BDD locale
     *
     * @param nameDatabase
     * @param loginDatabase
     * @param passwordDatabase
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException
     */
    public Connexion(String nameDatabase, String loginDatabase, String passwordDatabase) throws SQLException, ClassNotFoundException {
        // chargement driver "com.mysql.jdbc.Driver"
        Class.forName("com.mysql.jdbc.Driver");

        System.out.println("Driver OK!");
        String urlDatabase = "jdbc:mysql://localhost/" + nameDatabase;
        // url de connexion "jdbc:mysql://localhost:3305/usernameECE"
        
        if(passwordDatabase.equals("")){
        //String urlDatabase = "jdbc:mysql://localhost:8889/" + nameDatabase;
             urlDatabase = "jdbc:mysql://localhost/" + nameDatabase;
        }
        
        else if(passwordDatabase.equals("root")){
            urlDatabase = "jdbc:mysql://localhost:8889/" + nameDatabase;
        }

        //crÃ©ation d'une connexion JDBC Ã  la base 
        conn = DriverManager.getConnection(urlDatabase, loginDatabase, passwordDatabase);

        // crÃ©ation d'un ordre SQL (statement)
        stmt = conn.createStatement();
    }

    /**
     * Constructeur avec 4 paramÃ¨tres : username et password ECE, login et
     * password de la BDD Ã  distance sur le serveur de l'ECE
     * @param usernameECE
     * @param passwordECE
     * @param loginDatabase
     * @param passwordDatabase
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException
     */
    public Connexion(String usernameECE, String passwordECE, String loginDatabase, String passwordDatabase) throws SQLException, ClassNotFoundException {
        // chargement driver "com.mysql.jdbc.Driver"
        Class.forName("com.mysql.jdbc.Driver");

        // Connexion via le tunnel SSH avec le username et le password ECE
        SSHTunnel ssh = new SSHTunnel(usernameECE, passwordECE);

        if (ssh.connect()) {
            System.out.println("Connexion reussie");

            // url de connexion "jdbc:mysql://localhost:3305/usernameECE"
            String urlDatabase = "jdbc:mysql://localhost:3305/" + usernameECE;

            //crÃ©ation d'une connexion JDBC Ã  la base
            conn = DriverManager.getConnection(urlDatabase, loginDatabase, passwordDatabase);

            // crÃ©ation d'un ordre SQL (statement)
            stmt = conn.createStatement();

        }
    }

    /**
     * MÃ©thode qui ajoute la table en parametre dans son ArrayList
     *
     * @param table
     */
    public void ajouterTable(String table) {
        tables.add(table);
    }

    /**
     * MÃ©thode qui ajoute la requete de selection en parametre dans son
     * ArrayList
     *
     * @param requete
     */
    public void ajouterRequete(String requete) {
        requetes.add(requete);
    }

    /**
     * MÃ©thode qui ajoute la requete de MAJ en parametre dans son
     * ArrayList
     *
     * @param requete
     */
    public void ajouterRequeteMaj(String requete) {
        requetesMaj.add(requete);
    }

    /**
     * MÃ©thode qui retourne l'ArrayList des champs de la table en parametre
     *
     * @param table
     * @return
     * @throws java.sql.SQLException
     */
    public ArrayList remplirChampsTable(String table) throws SQLException {
        // rÃ©cupÃ©ration de l'ordre de la requete
        rset = stmt.executeQuery("select * from " + table);

        // rÃ©cupÃ©ration du rÃ©sultat de l'ordre
        rsetMeta = rset.getMetaData();

        // calcul du nombre de colonnes du resultat
        int nbColonne = rsetMeta.getColumnCount();

        // creation d'une ArrayList de String
        ArrayList<String> liste;
        liste = new ArrayList<>();
        String champs = "";
        // Ajouter tous les champs du resultat dans l'ArrayList
        for (int i = 0; i < nbColonne; i++) {
            champs = champs + " " + rsetMeta.getColumnLabel(i + 1);
        }

        // ajouter un "\n" Ã  la ligne des champs
        champs = champs + "\n";

        // ajouter les champs de la ligne dans l'ArrayList
        liste.add(champs);

        // Retourner l'ArrayList
        return liste;
    }

    /**
     * Methode qui retourne l'ArrayList des champs de la requete en parametre
     * @param requete
     * @return 
     * @throws java.sql.SQLException
     */
    public ArrayList remplirChampsRequete(String requete) throws SQLException {
        // rÃ©cupÃ©ration de l'ordre de la requete
        rset = stmt.executeQuery(requete);

        // rÃ©cupÃ©ration du rÃ©sultat de l'ordre
        rsetMeta = rset.getMetaData();

        // calcul du nombre de colonnes du resultat
        int nbColonne = rsetMeta.getColumnCount();

        // creation d'une ArrayList de String
        ArrayList<String> liste;
        liste = new ArrayList<String>();

        // tant qu'il reste une ligne 
        while (rset.next()) {
            String champs;
            champs = rset.getString(1); // ajouter premier champ

            // Concatener les champs de la ligne separes par ,
            for (int i = 1; i < nbColonne; i++) {
                champs = champs + "," + rset.getString(i + 1);
            }

            // ajouter un "\n" Ã  la ligne des champs
            champs = champs + "\n";

            // ajouter les champs de la ligne dans l'ArrayList
            liste.add(champs);
        }

        // Retourner l'ArrayList
        return liste;
    }

    /**
     * MÃ©thode qui execute une requete de MAJ en parametre
     * @param requeteMaj
     * @throws java.sql.SQLException
     */
    public void executeUpdate(String requeteMaj) throws SQLException {
        stmt.executeUpdate(requeteMaj);
    }
    
   /*
    public void Connexion(String id_ece, String mdp_ece, String id_bdd,String mdp_bdd) {      
        try {           
            //En local
            String name = "hopital";
            String login = "root";
            String passwd = "";
            
            
            //En distant
            
            String id_ece = "db151854"; //Mettre ici votre identifiant ECE
            String mdp_ece = "____"; //Mettre ici votre mot de passe ECE
            String id_bdd = "db151854-rw"; //Mettre ici votre identifiant sql.ece.fr
            String mdp_bdd = "_____"; //Mettre ici votre mot de passe sql.ece.fr
            
            Connexion connexion_locale = new Connexion(name,login,passwd);
            //Connexion connexion_distante = new Connexion(id_ece,mdp_ece,id_bdd,mdp_bdd);

            System.out.println("Connexion effective !");
            
            // Test d'affichage du contenu de la table "chambre" 
            
            //Création d'un objet Statement
            Statement state = connexion_locale.conn.createStatement();
            //Statement state = connexion_distante.conn.createStatement();
            //L'objet ResultSet contient le résultat de la requête SQL
            ResultSet result = state.executeQuery("SELECT * FROM chambre");
            //On récupère les MetaData
            ResultSetMetaData resultMeta = result.getMetaData();
         
            System.out.println("\n**********************************");
            //On affiche le nom des colonnes
            for(int i = 1; i <= resultMeta.getColumnCount(); i++)
                System.out.print("\t" + resultMeta.getColumnName(i).toUpperCase() + "\t *");
         
            System.out.println("\n**********************************");
         
            while(result.next()){         
                for(int i = 1; i <= resultMeta.getColumnCount(); i++)
                    System.out.print("\t" + result.getObject(i).toString() + "\t |");
            
                System.out.println("\n---------------------------------");

            }

            result.close();
            state.close();
         
            
        } catch (Exception e) {
            e.printStackTrace();
        }      
    }*/
}
