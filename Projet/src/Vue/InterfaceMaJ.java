/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vue;

import Controler.Maj;
import Controler.Recherche;
import Modele.Connexion;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.*;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maxime
 */
public class InterfaceMaJ extends JPanel implements ActionListener {
    
    //Déclaration des variables globales (champs, labels, etc)
    private JTextField champ1 = new JTextField();
    private JTextField champ2 = new JTextField();
    private JTextField champ3 = new JTextField();
    private JTextField champ4 = new JTextField();
    private JTextField champ5 = new JTextField();
    private JTextField champ6 = new JTextField();
    
    private JTextField champ7 = new JTextField();
    private JTextField champ8 = new JTextField();
    private JTextField champ9 = new JTextField();
    private JTextField champ10 = new JTextField();
    private JTextField champ11 = new JTextField();
    private JTextField champ12 = new JTextField();
    
    private JTextField champ1_infirm = new JTextField();
    private JTextField champ2_infirm = new JTextField();
    private JTextField champ3_infirm = new JTextField();
    private JComboBox champ1_doct = new JComboBox();
    
    private JTextField champ1_modif = new JTextField();
    private JTextField champ2_modif = new JTextField();
    private JTextField champ3_modif = new JTextField();
    private JTextField champ4_modif = new JTextField();
    private JTextField champ5_modif = new JTextField();
    private JTextField champ6_modif = new JTextField();
    
    
    private JLabel lab1 = new JLabel();
    private JLabel lab2 = new JLabel();
    private JLabel lab3 = new JLabel();
    private JLabel lab4 = new JLabel();
    private JLabel lab5 = new JLabel();
    private JLabel lab6 = new JLabel();
    
    private JLabel lab7 = new JLabel();
    private JLabel lab8 = new JLabel();
    private JLabel lab9 = new JLabel();
    private JLabel lab10 = new JLabel();
    private JLabel lab11 = new JLabel();
    private JLabel lab12 = new JLabel();
    
    private JLabel lab1_infirm = new JLabel("Code service : ");
    private JLabel lab2_infirm = new JLabel("Rotation : ");
    private JLabel lab3_infirm = new JLabel("Salaire : ");
    private JLabel lab1_doct = new JLabel("Specialité : ");
    
    private JLabel lab1_modif = new JLabel();
    private JLabel lab2_modif = new JLabel();
    private JLabel lab3_modif = new JLabel();
    private JLabel lab4_modif = new JLabel();
    private JLabel lab5_modif = new JLabel();
    private JLabel lab6_modif = new JLabel();
    
    private JCheckBox check1 = new JCheckBox("Infirmier");
    private JCheckBox check2 = new JCheckBox("Docteur");
    
    private JComboBox tableBox = new JComboBox();
    private JComboBox tableBox2 = new JComboBox();
    private JComboBox tableBox3 = new JComboBox();
    private JButton valider = new JButton("Valider l'ajout");
    private JButton valider2 = new JButton("Valider la suppression");  
    private JButton valider3 = new JButton("Valider la modification");
    private JButton valider_modif = new JButton("Valider");
    
    private JPanel Recherche;
    private JPanel Recherche2;
    private JPanel Recherche3;
    private JPanel Valid;
    private JPanel Valid2;
    private JPanel Valid3;
    
    private JPanel Ajout;
    private JPanel Supprim;
    private JPanel Modif;
    
    private JButton sous_menu_ajout = new JButton("Ajouter");
    private JButton sous_menu_supprim = new JButton("Supprimer");
    private JButton sous_menu_modif = new JButton("Modifier");
    
    
    private String table;
    private String table2;
    private String table3;
    private String cle1=null;
    private String cle2=null;
    
    private Maj maj = new Maj();
    Connexion connexion;
    
    public InterfaceMaJ(Connexion c) {
        connexion = c;
        String[] tab = {"","chambre", "employe","malade","hospitalisation","soigne"};
        String[] tab2 = {"","chambre","employe","malade","hospitalisation","soigne"};
        String[] tab3 = {"Anesthesiste","Cardiologue","Generaliste","Orthopediste","Pneumologue","Radiologue","Traumatologue"};
        String[] tab4 = {"","chambre","docteur","employe","malade","hospitalisation","infirmier","service","soigne"};
        
        /* AJOUT */
        
        //On initialise la taille des champ et des labels
        champ1.setPreferredSize(new Dimension(150, 27));
        champ2.setPreferredSize(new Dimension(150, 27));
        champ3.setPreferredSize(new Dimension(150, 27));
        champ4.setPreferredSize(new Dimension(150, 27));
        champ5.setPreferredSize(new Dimension(150, 27));
        champ6.setPreferredSize(new Dimension(150, 27));
        champ7.setPreferredSize(new Dimension(150, 27));
        champ8.setPreferredSize(new Dimension(150, 27));
        champ9.setPreferredSize(new Dimension(150, 27));
        champ10.setPreferredSize(new Dimension(150, 27));
        champ11.setPreferredSize(new Dimension(150, 27));
        champ12.setPreferredSize(new Dimension(150, 27));
        champ1_infirm.setPreferredSize(new Dimension(150, 27));
        champ2_infirm.setPreferredSize(new Dimension(150, 27));
        champ3_infirm.setPreferredSize(new Dimension(150, 27));
        champ1_doct.setPreferredSize(new Dimension(150, 27));
        champ1_modif.setPreferredSize(new Dimension(150, 27));
        lab1.setPreferredSize(new Dimension(150, 27));
        lab2.setPreferredSize(new Dimension(150, 27));
        lab3.setPreferredSize(new Dimension(150, 27));
        lab4.setPreferredSize(new Dimension(150, 27));
        lab5.setPreferredSize(new Dimension(150, 27));
        lab6.setPreferredSize(new Dimension(150, 27));
        lab7.setPreferredSize(new Dimension(150, 27));
        lab8.setPreferredSize(new Dimension(150, 27));
        lab9.setPreferredSize(new Dimension(150, 27));
        lab10.setPreferredSize(new Dimension(150, 27));
        lab11.setPreferredSize(new Dimension(150, 27));
        lab12.setPreferredSize(new Dimension(150, 27));
        lab1_infirm.setPreferredSize(new Dimension(150, 27));
        lab2_infirm.setPreferredSize(new Dimension(150, 27));
        lab3_infirm.setPreferredSize(new Dimension(150, 27));
        lab1_doct.setPreferredSize(new Dimension(150, 27));
        lab1_modif.setPreferredSize(new Dimension(150, 27));
        
        champ1_doct = new JComboBox(tab3);
        
        //Panel 1 : Bases
        JPanel InfoTable = new JPanel();
        InfoTable.setBackground(Color.white);
        InfoTable.setPreferredSize(new Dimension(400, 60));
        tableBox = new JComboBox(tab);
        tableBox.setPreferredSize(new Dimension(250, 20));
        InfoTable.setBorder(BorderFactory.createTitledBorder("Dans quelle table voulez-vous ajouter"));
        tableBox.addActionListener(this);
        InfoTable.add(tableBox);
        
        //AJOUT
        Ajout = new JPanel();
        Ajout.setBackground(Color.white);
        Ajout.setPreferredSize(new Dimension(400, 60));
        Ajout.setBorder(BorderFactory.createTitledBorder("Veuillez saisir les informations pour l'ajout"));
        Ajout.setLayout(null);
        Ajout.setVisible(true);
        
        //On ajout les champs et les labels
        Ajout.add(champ1);
        Ajout.add(champ2);
        Ajout.add(champ3);
        Ajout.add(champ4);
        Ajout.add(champ5);
        Ajout.add(champ6);
        Ajout.add(champ1_infirm);
        Ajout.add(champ2_infirm);
        Ajout.add(champ3_infirm);
        Ajout.add(champ1_doct);
        Ajout.add(lab1);
        Ajout.add(lab2);
        Ajout.add(lab3);
        Ajout.add(lab4);
        Ajout.add(lab5);
        Ajout.add(lab6);
        Ajout.add(lab1_infirm);
        Ajout.add(lab2_infirm);
        Ajout.add(lab3_infirm);
        Ajout.add(lab1_doct);
        Ajout.add(check1);
        Ajout.add(check2);
        check1.addActionListener(this);
        check2.addActionListener(this);
        
        //Par défaut, on les cache tous
        champ1.setVisible(false);
        champ2.setVisible(false);
        champ3.setVisible(false);
        champ4.setVisible(false);
        champ5.setVisible(false);
        champ6.setVisible(false);
        champ1_infirm.setVisible(false);
        champ2_infirm.setVisible(false);
        champ3_infirm.setVisible(false);
        champ1_doct.setVisible(false);
        lab1.setVisible(false);
        lab2.setVisible(false);
        lab3.setVisible(false);
        lab4.setVisible(false);
        lab5.setVisible(false);
        lab6.setVisible(false);
        lab1_infirm.setVisible(false);
        lab2_infirm.setVisible(false);
        lab3_infirm.setVisible(false);
        lab1_doct.setVisible(false);
        check1.setVisible(false);
        check2.setVisible(false);
        
        
        //On place les champs et les labels
        champ1.setBounds(300, 75, 200, 20);
        champ2.setBounds(300, 115, 200, 20);
        champ3.setBounds(300, 155, 200, 20);
        champ4.setBounds(300, 195, 200, 20);
        champ5.setBounds(300, 235, 200, 20);
        champ6.setBounds(300, 275, 200, 20);
        lab1.setBounds(50, 75, 200, 20);
        lab2.setBounds(50, 115, 200, 20);
        lab3.setBounds(50, 155, 200, 20);
        lab4.setBounds(50, 195, 200, 20);
        lab5.setBounds(50, 235, 200, 20);
        lab6.setBounds(50, 275, 200, 20);
        check1.setBounds(50, 315, 200, 20);
        check2.setBounds(350, 315, 200, 20);
        champ1_infirm.setBounds(200, 355, 200, 20);
        champ2_infirm.setBounds(200, 395, 200, 20);
        champ3_infirm.setBounds(200, 435, 200, 20);
        champ1_doct.setBounds(450, 355, 200, 20);
        lab1_infirm.setBounds(50, 355, 200, 20);
        lab2_infirm.setBounds(50, 395, 200, 20);
        lab3_infirm.setBounds(50, 435, 200, 20);
        lab1_doct.setBounds(350, 355, 200, 20);
        
        
        
        // Panel bouton validation 
        Valid = new JPanel();
        Valid.setBackground(Color.white);
        valider.addActionListener(this);
        Valid.add(valider);
             
        Recherche = new JPanel();
        Recherche.setBackground(Color.white);
        Recherche.add(InfoTable);
        
        /* SUPPRIM */
        
        //Panel 1 : Choix de la table où supprimer
        JPanel InfoTable2 = new JPanel();
        InfoTable2.setBackground(Color.white);
        InfoTable2.setPreferredSize(new Dimension(400, 60));
        tableBox2 = new JComboBox(tab2);
        tableBox2.setPreferredSize(new Dimension(250, 20));
        InfoTable2.setBorder(BorderFactory.createTitledBorder("Dans quelle table voulez-vous supprimer"));
        tableBox2.addActionListener(this);
        InfoTable2.add(tableBox2);
        
        //Panel pour la suppression
        Supprim = new JPanel();
        Supprim.setBackground(Color.white);
        Supprim.setPreferredSize(new Dimension(400, 60));
        Supprim.setBorder(BorderFactory.createTitledBorder("Veuillez saisir les informations pour la suppression"));
        Supprim.setLayout(null);
        Supprim.setVisible(true);
        
        Supprim.add(champ7);
        Supprim.add(champ8);
        Supprim.add(champ9);
        Supprim.add(champ10);
        Supprim.add(champ11);
        Supprim.add(champ12);
        Supprim.add(lab7);
        Supprim.add(lab8);
        Supprim.add(lab9);
        Supprim.add(lab10);
        Supprim.add(lab11);
        Supprim.add(lab12);
        
        //Par défaut, on les cache tous
        
        champ7.setVisible(false);
        champ8.setVisible(false);
        champ9.setVisible(false);
        champ10.setVisible(false);
        champ11.setVisible(false);
        champ12.setVisible(false);
        lab7.setVisible(false);
        lab8.setVisible(false);
        lab9.setVisible(false);
        lab10.setVisible(false);
        lab11.setVisible(false);
        lab12.setVisible(false);
        
        //On place les champs et les labels
        champ7.setBounds(300, 75, 200, 20);
        champ8.setBounds(300, 115, 200, 20);
        champ9.setBounds(300, 155, 200, 20);
        champ10.setBounds(300, 195, 200, 20);
        champ11.setBounds(300, 235, 200, 20);
        champ12.setBounds(300, 275, 200, 20);
        lab7.setBounds(50, 75, 200, 20);
        lab8.setBounds(50, 115, 200, 20);
        lab9.setBounds(50, 155, 200, 20);
        lab10.setBounds(50, 195, 200, 20);
        lab11.setBounds(50, 235, 200, 20);
        lab12.setBounds(50, 275, 200, 20);
        
        // Panel bouton validation 
        Valid2 = new JPanel();
        Valid2.setBackground(Color.white);
        valider2.addActionListener(this);
        Valid2.add(valider2);
             
        Recherche2 = new JPanel();
        Recherche2.setBackground(Color.white);
        Recherche2.add(InfoTable2);
        
        
        
        /* MODIF */
        
        JPanel InfoTable3 = new JPanel();
        InfoTable3.setBackground(Color.white);
        InfoTable3.setPreferredSize(new Dimension(400, 60));
        tableBox3 = new JComboBox(tab4);
        tableBox3.setPreferredSize(new Dimension(250, 20));
        InfoTable3.setBorder(BorderFactory.createTitledBorder("Dans quelle table voulez-vous modifier"));
        tableBox3.addActionListener(this);
        InfoTable3.add(tableBox3);
        
        //Panel pour la modification
        Modif = new JPanel();
        Modif.setBackground(Color.white);
        Modif.setPreferredSize(new Dimension(400, 60));
        Modif.setBorder(BorderFactory.createTitledBorder("Veuillez saisir les informations pour la modification"));
        Modif.setLayout(null);
        Modif.setVisible(true);
        
        Modif.add(champ1_modif);
        Modif.add(champ2_modif);
        Modif.add(champ3_modif);
        Modif.add(champ4_modif);
        Modif.add(champ5_modif);
        Modif.add(champ6_modif);
        Modif.add(lab1_modif);
        Modif.add(lab2_modif);
        Modif.add(lab3_modif);
        Modif.add(lab4_modif);
        Modif.add(lab5_modif);
        Modif.add(lab6_modif);
        Modif.add(valider_modif);
        valider_modif.addActionListener(this);
        
        champ1_modif.setVisible(false);
        champ2_modif.setVisible(false);
        champ3_modif.setVisible(false);
        champ4_modif.setVisible(false);
        champ5_modif.setVisible(false);
        champ6_modif.setVisible(false);
        lab1_modif.setVisible(false);
        lab2_modif.setVisible(false);
        lab3_modif.setVisible(false);
        lab4_modif.setVisible(false);
        lab5_modif.setVisible(false);
        lab6_modif.setVisible(false);
        valider_modif.setVisible(false);
        
        champ1_modif.setBounds(300, 75, 200, 20);
        champ2_modif.setBounds(300, 115, 200, 20);
        champ3_modif.setBounds(300, 155, 200, 20);
        champ4_modif.setBounds(300, 195, 200, 20);
        champ5_modif.setBounds(300, 235, 200, 20);
        champ6_modif.setBounds(300, 275, 200, 20);
        lab1_modif.setBounds(50, 75, 200, 20);
        lab2_modif.setBounds(50, 115, 200, 20);
        lab3_modif.setBounds(50, 155, 200, 20);
        lab4_modif.setBounds(50, 195, 200, 20);
        lab5_modif.setBounds(50, 235, 200, 20);
        lab6_modif.setBounds(50, 275, 200, 20);
        valider_modif.setBounds(550, 75, 75, 20);
        
        
        // Panel bouton validation 
        Valid3 = new JPanel();
        Valid3.setBackground(Color.white);
        valider3.addActionListener(this);
        Valid3.add(valider3);
             
        Recherche3 = new JPanel();
        Recherche3.setBackground(Color.white);
        Recherche3.add(InfoTable3);
        
        
        /*SOUS_MENU */
        JPanel Sous_menu = new JPanel();
        Sous_menu.setPreferredSize(new Dimension(700, 50));
        sous_menu_ajout.addActionListener(this);
        sous_menu_supprim.addActionListener(this);
        sous_menu_modif.addActionListener(this);
        Sous_menu.add(sous_menu_ajout);
        Sous_menu.add(sous_menu_supprim);
        Sous_menu.add(sous_menu_modif);

        this.setLayout(null);
        this.add(Sous_menu);
        Sous_menu.setBounds(15, 10, 700, 50);
        this.setVisible(true);

        
    }
    
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        table = tableBox.getSelectedItem().toString();
        table2 = tableBox2.getSelectedItem().toString();
        table3 = tableBox3.getSelectedItem().toString();
        
        
        if(source == tableBox){

            //Si on veut insérer dans la classe chambre
            if(tableBox.getSelectedItem().equals("chambre")){
                
                //On change le nom des champs à saisir
                lab1.setText("Code service : ");
                lab2.setText("Numéro_chambre : ");
                lab3.setText("Surveillant : ");
                lab4.setText("Nombre de lits : ");

                //On rend visible les champ correspondant
                champ1.setVisible(true);
                champ2.setVisible(true);
                champ3.setVisible(true);
                champ4.setVisible(true);
                champ5.setVisible(false);
                champ6.setVisible(false);
                lab1.setVisible(true);
                lab2.setVisible(true);
                lab3.setVisible(true);
                lab4.setVisible(true);
                lab5.setVisible(false);
                lab6.setVisible(false);
                check1.setVisible(false);
                check2.setVisible(false);
                champ1_infirm.setVisible(false);
                champ2_infirm.setVisible(false);
                champ3_infirm.setVisible(false);
                champ1_doct.setVisible(false);
                lab1_infirm.setVisible(false);
                lab2_infirm.setVisible(false);
                lab3_infirm.setVisible(false);
                lab1_doct.setVisible(false);

            }


            //Si on veut insérer dans la classe employe
            if(tableBox.getSelectedItem().equals("employe")){

                //On change le nom des champs à saisir
                lab1.setText("Numero : ");
                lab2.setText("Nom : ");
                lab3.setText("Prenom : ");
                lab4.setText("Adresse : ");
                lab5.setText("Tel : ");

                //On rend visible les champ correspondant
                champ1.setVisible(true);
                champ2.setVisible(true);
                champ3.setVisible(true);
                champ4.setVisible(true);
                champ5.setVisible(true);
                champ6.setVisible(false);
                lab1.setVisible(true);
                lab2.setVisible(true);
                lab3.setVisible(true);
                lab4.setVisible(true);
                lab5.setVisible(true);
                lab6.setVisible(false);
                check1.setVisible(true);
                check2.setVisible(true);

            }

            //Si on veut insérer dans la classe hospitalisation
            if(tableBox.getSelectedItem().equals("hospitalisation")){

                //On change le nom des champs à saisir
                lab1.setText("Numero du malade : ");
                lab2.setText("Code du service : ");
                lab3.setText("Numero de chambre : ");
                lab4.setText("Lit : ");

                //On rend visible les champ correspondant
                champ1.setVisible(true);
                champ2.setVisible(true);
                champ3.setVisible(true);
                champ4.setVisible(true);
                champ5.setVisible(false);
                champ6.setVisible(false);
                lab1.setVisible(true);
                lab2.setVisible(true);
                lab3.setVisible(true);
                lab4.setVisible(true);
                lab5.setVisible(false);
                lab6.setVisible(false);
                check1.setVisible(false);
                check2.setVisible(false);             
                champ1_infirm.setVisible(false);
                champ2_infirm.setVisible(false);
                champ3_infirm.setVisible(false);
                champ1_doct.setVisible(false);
                lab1_infirm.setVisible(false);
                lab2_infirm.setVisible(false);
                lab3_infirm.setVisible(false);
                lab1_doct.setVisible(false);


            }

            //Si on veut insérer dans la classe malade
            if(tableBox.getSelectedItem().equals("malade")){

                //On change le nom des champs à saisir
                lab1.setText("Numero : ");
                lab2.setText("Nom : ");
                lab3.setText("Prenom : ");
                lab4.setText("Adresse : ");
                lab5.setText("Tel : ");
                lab6.setText("Mutuelle : ");

                //On rend visible les champ correspondant
                champ1.setVisible(true);
                champ2.setVisible(true);
                champ3.setVisible(true);
                champ4.setVisible(true);
                champ5.setVisible(true);
                champ6.setVisible(true);
                lab1.setVisible(true);
                lab2.setVisible(true);
                lab3.setVisible(true);
                lab4.setVisible(true);
                lab5.setVisible(true);
                lab6.setVisible(true);
                check1.setVisible(false);
                check2.setVisible(false);
                champ1_infirm.setVisible(false);
                champ2_infirm.setVisible(false);
                champ3_infirm.setVisible(false);
                champ1_doct.setVisible(false);
                lab1_infirm.setVisible(false);
                lab2_infirm.setVisible(false);
                lab3_infirm.setVisible(false);
                lab1_doct.setVisible(false);


            }

            

            //Si on veut insérer dans la classe soigne
            if(tableBox.getSelectedItem().equals("soigne")){

                //On change le nom des champs à saisir
                lab1.setText("Numero du docteur : ");
                lab2.setText("Numero du malade : ");


                //On rend visible les champ correspondant
                champ1.setVisible(true);
                champ2.setVisible(true);
                champ3.setVisible(false);
                champ4.setVisible(false);
                champ5.setVisible(false);
                champ6.setVisible(false);
                lab1.setVisible(true);
                lab2.setVisible(true);
                lab3.setVisible(false);
                lab4.setVisible(false);
                lab5.setVisible(false);
                lab6.setVisible(false);
                check1.setVisible(false);
                check2.setVisible(false);
                champ1_infirm.setVisible(false);
                champ2_infirm.setVisible(false);
                champ3_infirm.setVisible(false);
                champ1_doct.setVisible(false);
                lab1_infirm.setVisible(false);
                lab2_infirm.setVisible(false);
                lab3_infirm.setVisible(false);
                lab1_doct.setVisible(false);


            }
        
        }
        
        if(source == tableBox2){
            if(tableBox2.getSelectedItem().equals("chambre")){
                //On change le nom des champs à saisir
                lab7.setText("Code service : ");
                lab8.setText("Numéro_chambre : ");

                //On rend visible les champ correspondant
                champ7.setVisible(true);
                champ8.setVisible(true);
                champ9.setVisible(false);
                champ10.setVisible(false);
                champ11.setVisible(false);
                champ12.setVisible(false);
                lab7.setVisible(true);
                lab8.setVisible(true);
                lab9.setVisible(false);
                lab10.setVisible(false);
                lab11.setVisible(false);
                lab12.setVisible(false);
            }
            if(tableBox2.getSelectedItem().equals("employe")){
                //On change le nom des champs à saisir
                lab7.setText("Numero : ");

                //On rend visible les champ correspondant
                champ7.setVisible(true);
                champ8.setVisible(false);
                champ9.setVisible(false);
                champ10.setVisible(false);
                champ11.setVisible(false);
                champ12.setVisible(false);
                lab7.setVisible(true);
                lab8.setVisible(false);
                lab9.setVisible(false);
                lab10.setVisible(false);
                lab11.setVisible(false);
                lab12.setVisible(false);
            }
            if(tableBox2.getSelectedItem().equals("malade")){
                //On change le nom des champs à saisir
                lab7.setText("Numero : ");

                //On rend visible les champ correspondant
                champ7.setVisible(true);
                champ8.setVisible(false);
                champ9.setVisible(false);
                champ10.setVisible(false);
                champ11.setVisible(false);
                champ12.setVisible(false);
                lab7.setVisible(true);
                lab8.setVisible(false);
                lab9.setVisible(false);
                lab10.setVisible(false);
                lab11.setVisible(false);
                lab12.setVisible(false);
            }
            if(tableBox2.getSelectedItem().equals("hospitalisation")){
                //On change le nom des champs à saisir
                lab7.setText("Numero : ");

                //On rend visible les champ correspondant
                champ7.setVisible(true);
                champ8.setVisible(false);
                champ9.setVisible(false);
                champ10.setVisible(false);
                champ11.setVisible(false);
                champ12.setVisible(false);
                lab7.setVisible(true);
                lab8.setVisible(false);
                lab9.setVisible(false);
                lab10.setVisible(false);
                lab11.setVisible(false);
                lab12.setVisible(false);
            }
            if(tableBox2.getSelectedItem().equals("soigne")){
                //On change le nom des champs à saisir
                lab7.setText("Numero du docteur : ");
                lab8.setText("Numero du malade : ");

                //On rend visible les champ correspondant
                champ7.setVisible(true);
                champ8.setVisible(true);
                champ9.setVisible(false);
                champ10.setVisible(false);
                champ11.setVisible(false);
                champ12.setVisible(false);
                lab7.setVisible(true);
                lab8.setVisible(true);
                lab9.setVisible(false);
                lab10.setVisible(false);
                lab11.setVisible(false);
                lab12.setVisible(false);
            }
            
        }
        
        if(source == tableBox3){
            if(tableBox3.getSelectedItem().equals("chambre")){
                lab1_modif.setText("Code du service : ");
                lab2_modif.setText("Numero de chambre : ");
                champ1_modif.setText("");
                champ2_modif.setText("");

                //On rend visible les champ correspondant
                champ1_modif.setVisible(true);
                champ2_modif.setVisible(true);
                champ3_modif.setVisible(false);
                champ4_modif.setVisible(false);
                champ5_modif.setVisible(false);
                champ6_modif.setVisible(false);
                lab1_modif.setVisible(true);
                lab2_modif.setVisible(true);
                lab3_modif.setVisible(false);
                lab4_modif.setVisible(false);
                lab5_modif.setVisible(false);
                lab6_modif.setVisible(false);
                valider_modif.setVisible(true);
            }
            if(tableBox3.getSelectedItem().equals("docteur")){
                lab1_modif.setText("Numero : ");
                champ1_modif.setText("");
                champ2_modif.setText("");

                //On rend visible les champ correspondant
                champ1_modif.setVisible(true);
                champ2_modif.setVisible(false);
                champ3_modif.setVisible(false);
                champ4_modif.setVisible(false);
                champ5_modif.setVisible(false);
                champ6_modif.setVisible(false);
                lab1_modif.setVisible(true);
                lab2_modif.setVisible(false);              
                lab3_modif.setVisible(false);              
                lab4_modif.setVisible(false);                
                lab5_modif.setVisible(false);
                lab6_modif.setVisible(false);
                valider_modif.setVisible(true);
            }
            if(tableBox3.getSelectedItem().equals("employe")){
                lab1_modif.setText("Numero : ");
                champ1_modif.setText("");
                champ2_modif.setText("");

                //On rend visible les champ correspondant
                champ1_modif.setVisible(true);
                lab1_modif.setVisible(true);
                champ2_modif.setVisible(false);
                champ3_modif.setVisible(false);
                champ4_modif.setVisible(false);
                champ5_modif.setVisible(false);
                champ6_modif.setVisible(false);
                lab2_modif.setVisible(false);
                lab3_modif.setVisible(false);              
                lab4_modif.setVisible(false);                
                lab5_modif.setVisible(false);
                lab6_modif.setVisible(false);
                valider_modif.setVisible(true);
            }
            if(tableBox3.getSelectedItem().equals("hospitalisation")){
                lab1_modif.setText("Numero du malade : ");
                champ1_modif.setText("");
                champ2_modif.setText("");

                //On rend visible les champ correspondant
                champ1_modif.setVisible(true);
                lab1_modif.setVisible(true);
                champ2_modif.setVisible(false);
                champ3_modif.setVisible(false);
                champ4_modif.setVisible(false);
                champ5_modif.setVisible(false);
                champ6_modif.setVisible(false);
                lab2_modif.setVisible(false);
                lab3_modif.setVisible(false);              
                lab4_modif.setVisible(false);                
                lab5_modif.setVisible(false);
                lab6_modif.setVisible(false);
                valider_modif.setVisible(true);
            }
            if(tableBox3.getSelectedItem().equals("infirmier")){
                lab1_modif.setText("Numero : ");
                champ1_modif.setText("");
                champ2_modif.setText("");

                //On rend visible les champ correspondant
                champ1_modif.setVisible(true);
                lab1_modif.setVisible(true);
                champ2_modif.setVisible(false);
                champ3_modif.setVisible(false);
                champ4_modif.setVisible(false);
                champ5_modif.setVisible(false);
                champ6_modif.setVisible(false);
                lab2_modif.setVisible(false);
                lab3_modif.setVisible(false);              
                lab4_modif.setVisible(false);                
                lab5_modif.setVisible(false);
                lab6_modif.setVisible(false);
                valider_modif.setVisible(true);
            }
            if(tableBox3.getSelectedItem().equals("malade")){
                lab1_modif.setText("Numero du malade : ");
                champ1_modif.setText("");
                champ2_modif.setText("");

                //On rend visible les champ correspondant
                champ1_modif.setVisible(true);
                lab1_modif.setVisible(true);
                champ2_modif.setVisible(false);
                champ3_modif.setVisible(false);
                champ4_modif.setVisible(false);
                champ5_modif.setVisible(false);
                champ6_modif.setVisible(false);
                lab2_modif.setVisible(false);
                lab3_modif.setVisible(false);              
                lab4_modif.setVisible(false);                
                lab5_modif.setVisible(false);
                lab6_modif.setVisible(false);
                valider_modif.setVisible(true);
            }
            if(tableBox3.getSelectedItem().equals("service")){
                lab1_modif.setText("Code : ");
                champ1_modif.setText("");
                champ2_modif.setText("");

                //On rend visible les champ correspondant
                champ1_modif.setVisible(true);
                lab1_modif.setVisible(true);
                champ2_modif.setVisible(false);
                champ3_modif.setVisible(false);
                champ4_modif.setVisible(false);
                champ5_modif.setVisible(false);
                champ6_modif.setVisible(false);
                lab2_modif.setVisible(false);
                lab3_modif.setVisible(false);              
                lab4_modif.setVisible(false);                
                lab5_modif.setVisible(false);
                lab6_modif.setVisible(false);
                valider_modif.setVisible(true);
            }
            if(tableBox3.getSelectedItem().equals("soigne")){
                lab1_modif.setText("Numero du docteur : ");
                lab2_modif.setText("Numero du malade : ");
                champ1_modif.setText("");
                champ2_modif.setText("");

                //On rend visible les champ correspondant
                champ1_modif.setVisible(true);
                champ2_modif.setVisible(true);
                champ3_modif.setVisible(false);
                champ4_modif.setVisible(false);
                champ5_modif.setVisible(false);
                champ6_modif.setVisible(false);
                lab1_modif.setVisible(true);
                lab2_modif.setVisible(true);
                lab3_modif.setVisible(false);              
                lab4_modif.setVisible(false);                
                lab5_modif.setVisible(false);
                lab6_modif.setVisible(false);
                valider_modif.setVisible(true);
            }
        }
        
        if(source == check1){
            check1.setSelected(true);
            check2.setSelected(false);
            champ1_infirm.setVisible(true);
            champ2_infirm.setVisible(true);
            champ3_infirm.setVisible(true);
            champ1_doct.setVisible(false);
            lab1_infirm.setVisible(true);
            lab2_infirm.setVisible(true);
            lab3_infirm.setVisible(true);
            lab1_doct.setVisible(false);
        }
        if(source == check2){
            check1.setSelected(false);
            check2.setSelected(true);
            champ1_infirm.setVisible(false);
            champ2_infirm.setVisible(false);
            champ3_infirm.setVisible(false);
            champ1_doct.setVisible(true);
            lab1_infirm.setVisible(false);
            lab2_infirm.setVisible(false);
            lab3_infirm.setVisible(false);
            lab1_doct.setVisible(true);
        }
        
        if(source == sous_menu_ajout){   
            
            Recherche3.setVisible(false);
            Modif.setVisible(false);
            Valid3.setVisible(false);
            
            Recherche2.setVisible(false);
            Supprim.setVisible(false);
            Valid2.setVisible(false);
            
            Recherche.setVisible(true);
            Ajout.setVisible(true);
            Valid.setVisible(true);
            
            this.add(Recherche);
            Recherche.setBounds(15, 60, 700, 75);
            this.add(Ajout);
            this.add(Valid);
            Ajout.setBounds(15, 135, 700, 600);
            Valid.setBounds(15, 735, 700, 50);
            validate();
        }
        
        if(source == sous_menu_supprim){   
            
            Recherche3.setVisible(false);
            Modif.setVisible(false);
            Valid3.setVisible(false);
            
            Recherche.setVisible(false);
            Ajout.setVisible(false);
            Valid.setVisible(false);
            
            Recherche2.setVisible(true);
            Supprim.setVisible(true);
            Valid2.setVisible(true);
            
            this.add(Recherche2);
            Recherche2.setBounds(15, 60, 700, 75);
            this.add(Supprim); 
            this.add(Valid2);  

            Supprim.setBounds(15, 135, 700, 600);
            Valid2.setBounds(15, 735, 700, 50);
            validate();
        }
        
        if(source == sous_menu_modif){
            
            Recherche.setVisible(false);
            Ajout.setVisible(false);
            Valid.setVisible(false);
            
            Recherche2.setVisible(false);
            Supprim.setVisible(false);
            Valid2.setVisible(false);
            
            Recherche3.setVisible(true);
            Modif.setVisible(true);
            Valid3.setVisible(true);
            
            this.add(Recherche3);
            Recherche3.setBounds(15, 60, 700, 75);
            this.add(Modif); 
            this.add(Valid3);  

            Modif.setBounds(15, 135, 700, 600);
            Valid3.setBounds(15, 735, 700, 50);
            validate();
        }
        
        if(source == valider){
            
            String table = tableBox.getSelectedItem().toString();
            String query = null;
            
            if(table.equals("chambre")){
                String code_service = champ1.getText();
                String no_chambre = champ2.getText();
                String surveillant = champ3.getText();
                String nb_lits = champ4.getText();
                query = "INSERT INTO chambre VALUES('"+code_service+"','"+no_chambre+"','"+surveillant+"','"+nb_lits+"')" ;
                maj.Insert(connexion, query);
            }
            if(table.equals("employe")){
                String numero = champ1.getText();
                String nom = champ2.getText();
                String prenom = champ3.getText();
                String adresse = champ4.getText();
                String tel = champ5.getText();
                String code_service;
                String rotation;
                String salaire;
                String specialite;
                
                query = "INSERT INTO employe VALUES('"+numero+"','"+nom+"','"+prenom+"','"+adresse+"','"+tel+"')" ;
                maj.Insert(connexion, query);
                
                if(check1.isSelected()==true){
                    code_service = champ1_infirm.getText();
                    rotation = champ2_infirm.getText();
                    salaire = champ3_infirm.getText();
                    query = "INSERT INTO infirmier VALUES('"+numero+"','"+code_service+"','"+rotation+"','"+salaire+"')" ;
                    maj.Insert(connexion, query);
                }
                if(check2.isSelected()==true){
                    specialite = champ1_doct.getSelectedItem().toString();
                    query = "INSERT INTO docteur VALUES('"+numero+"','"+specialite+"')" ;
                    maj.Insert(connexion, query);
                }
            }
            if(table.equals("hospitalisation")){
                String no_malade = champ1.getText();
                String code_service = champ2.getText();
                String no_chambre = champ3.getText();
                String lit = champ4.getText();
                query = "INSERT INTO hospitalisation VALUES('"+no_malade+"','"+code_service+"','"+no_chambre+"','"+lit+"')" ;
                maj.Insert(connexion, query);
            }
            if(table.equals("malade")){
                String numero = champ1.getText();
                String nom = champ2.getText();
                String prenom = champ3.getText();
                String adresse = champ4.getText();
                String tel = champ5.getText();
                String mutuelle = champ6.getText();
                query = "INSERT INTO malade VALUES('"+numero+"','"+nom+"','"+prenom+"','"+adresse+"','"+tel+"','"+mutuelle+"')" ;
                maj.Insert(connexion, query);
            }
           
            if(table.equals("soigne")){
                String no_docteur = champ1.getText();
                String no_malade = champ2.getText();
                query = "INSERT INTO soigne VALUES('"+no_docteur+"','"+no_malade+"')" ;
                maj.Insert(connexion, query);
            }
            
            

        }
        
        if(source == valider2){
            String query = null;
            
            if(table2.equals("chambre")){
                String code_service = champ7.getText();
                String no_chambre = champ8.getText();
                query = "DELETE from chambre WHERE code_service='"+code_service+"'"+"AND no_chambre ='"+no_chambre+"'";
            }
            if(table2.equals("employe")){
                String numero = champ7.getText();
                query = "DELETE from employe WHERE numero='"+numero+"'";
            }
            if(table2.equals("hospitalisation")){
                String no_malade = champ7.getText();
                query = "DELETE from hospitalisation WHERE no_malade='"+no_malade+"'";
            }
            if(table2.equals("malade")){
                String numero = champ7.getText();
                query = "DELETE from malade WHERE numero='"+numero+"'";         
            }
            if(table2.equals("soigne")){
                String no_docteur = champ7.getText();
                String no_malade = champ8.getText();
                query = "DELETE from soigne WHERE no_docteur='"+no_docteur+"'"+"AND no_malade ='"+no_malade+"'";         
            }
            
            maj.Delete(connexion, query);
        }
        
        if(source == valider_modif){
            String query = null;
            ResultSet result = null;
            
            if(table3.equals("chambre")){
                cle1=champ1_modif.getText();
                cle2=champ2_modif.getText();
                lab3_modif.setText("Surveillant : ");
                lab4_modif.setText("Nombre de lits : ");
                champ3_modif.setVisible(true);
                champ4_modif.setVisible(true);
                champ5_modif.setVisible(false);
                champ6_modif.setVisible(false);
                lab3_modif.setVisible(true);
                lab4_modif.setVisible(true);
                lab5_modif.setVisible(false);
                lab6_modif.setVisible(false);

                
                String code_service = champ1_modif.getText();
                String no_chambre = champ2_modif.getText();
                String[] surveillant=new String [10];
                String[] nb_lits=new String [10];
                int i = 0;
                
                try {
                    
                Statement state = connexion.getConn().createStatement();
                result = state.executeQuery("SELECT * from chambre WHERE code_service='"+code_service+"'"+"AND no_chambre ='"+no_chambre+"'");
                
                
                while(result.next()){
                    surveillant[i] = result.getString("surveillant");
                    nb_lits[i] = result.getString("nb_lits");
                    i++;  
                }
                } catch (SQLException ex) {
                Logger.getLogger(InterfaceMaJ.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                champ3_modif.setText(surveillant[0]);
                champ4_modif.setText(nb_lits[0]);
                
                
            }
            if(table3.equals("docteur")){
                cle1=champ1_modif.getText();
                lab2_modif.setText("Specialite : ");
                champ2_modif.setVisible(true);
                champ3_modif.setVisible(false);
                champ4_modif.setVisible(false);
                champ5_modif.setVisible(false);
                champ6_modif.setVisible(false);
                lab2_modif.setVisible(true);
                lab3_modif.setVisible(false);
                lab4_modif.setVisible(false);
                lab5_modif.setVisible(false);
                lab6_modif.setVisible(false);

                
                String numero = champ1_modif.getText();
                String[] specialite=new String [10];
                int i = 0;
                
                try {
                    
                Statement state = connexion.getConn().createStatement();
                result = state.executeQuery("SELECT * from docteur WHERE numero='"+numero+"'");
                
                
                while(result.next()){
                    specialite[i] = result.getString("specialite");
                    i++;  
                }
                } catch (SQLException ex) {
                Logger.getLogger(InterfaceMaJ.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                champ2_modif.setText(specialite[0]);   
            }
            if(table3.equals("employe")){    
                cle1=champ1_modif.getText();
                lab2_modif.setText("Nom : ");
                lab3_modif.setText("Prenom : ");
                lab4_modif.setText("Adresse : ");
                lab5_modif.setText("Tel : ");
                champ2_modif.setVisible(true);
                champ3_modif.setVisible(true);
                champ4_modif.setVisible(true);
                champ5_modif.setVisible(true);
                champ6_modif.setVisible(false);
                lab2_modif.setVisible(true);
                lab3_modif.setVisible(true);
                lab4_modif.setVisible(true);
                lab5_modif.setVisible(true);
                lab6_modif.setVisible(false);

                
                String numero = champ1_modif.getText();
                String[] nom=new String [10];
                String[] prenom=new String [10];              
                String[] adresse=new String [10];
                String[] tel=new String [10];
                int i = 0;
                
                try {
                    
                Statement state = connexion.getConn().createStatement();
                result = state.executeQuery("SELECT * from employe WHERE numero='"+numero+"'");
                
                
                while(result.next()){
                    nom[i] = result.getString("nom");
                    prenom[i] = result.getString("prenom");
                    adresse[i] = result.getString("adresse");
                    tel[i] = result.getString("tel");
                    i++;  
                }
                } catch (SQLException ex) {
                Logger.getLogger(InterfaceMaJ.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                champ2_modif.setText(nom[0]);
                champ3_modif.setText(prenom[0]);
                champ4_modif.setText(adresse[0]);
                champ5_modif.setText(tel[0]);
                
                
            }
            if(table3.equals("hospitalisation")){
                cle1=champ1_modif.getText();
                lab2_modif.setText("Code du service : ");
                lab3_modif.setText("Numero de chambre : ");
                lab4_modif.setText("Lit : ");
                champ2_modif.setVisible(true);
                champ3_modif.setVisible(true);
                champ4_modif.setVisible(true);
                champ5_modif.setVisible(false);
                champ6_modif.setVisible(false);
                lab2_modif.setVisible(true);
                lab3_modif.setVisible(true);
                lab4_modif.setVisible(true);
                lab5_modif.setVisible(false);
                lab6_modif.setVisible(false);

                
                String no_malade = champ1_modif.getText();
                String[] code_service=new String [10];
                String[] no_chambre=new String [10];              
                String[] lit=new String [10];
                int i = 0;
                
                try {
                    
                Statement state = connexion.getConn().createStatement();
                result = state.executeQuery("SELECT * from hospitalisation WHERE no_malade='"+no_malade+"'");
                
                
                while(result.next()){
                    code_service[i] = result.getString("code_service");
                    no_chambre[i] = result.getString("no_chambre");
                    lit[i] = result.getString("lit");
                    i++;  
                }
                } catch (SQLException ex) {
                Logger.getLogger(InterfaceMaJ.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                champ2_modif.setText(code_service[0]);
                champ3_modif.setText(no_chambre[0]);
                champ4_modif.setText(lit[0]);
                
                
            }
            if(table3.equals("infirmier")){       
                cle1=champ1_modif.getText();
                lab2_modif.setText("Code du service : ");
                lab3_modif.setText("Rotation : ");
                lab4_modif.setText("Salaire : ");
                champ2_modif.setVisible(true);
                champ3_modif.setVisible(true);
                champ4_modif.setVisible(true);
                champ5_modif.setVisible(false);
                champ6_modif.setVisible(false);
                lab2_modif.setVisible(true);
                lab3_modif.setVisible(true);
                lab4_modif.setVisible(true);
                lab5_modif.setVisible(false);
                lab6_modif.setVisible(false);

                
                String numero = champ1_modif.getText();
                String[] code_service=new String [10];
                String[] rotation=new String [10];              
                String[] salaire=new String [10];
                int i = 0;
                
                try {
                    
                Statement state = connexion.getConn().createStatement();
                result = state.executeQuery("SELECT * from infirmier WHERE numero='"+numero+"'");
                
                
                while(result.next()){
                    code_service[i] = result.getString("code_service");
                    rotation[i] = result.getString("rotation");
                    salaire[i] = result.getString("salaire");
                    i++;  
                }
                } catch (SQLException ex) {
                Logger.getLogger(InterfaceMaJ.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                champ2_modif.setText(code_service[0]);
                champ3_modif.setText(rotation[0]);
                champ4_modif.setText(salaire[0]);
                
                
            }
            if(table3.equals("malade")){       
                cle1=champ1_modif.getText();
                lab2_modif.setText("Nom : ");
                lab3_modif.setText("Prenom : ");
                lab4_modif.setText("Adresse : ");
                lab5_modif.setText("Tel : ");
                lab6_modif.setText("Mutuelle : ");
                champ2_modif.setVisible(true);
                champ3_modif.setVisible(true);
                champ4_modif.setVisible(true);
                champ5_modif.setVisible(true);
                champ6_modif.setVisible(true);
                lab2_modif.setVisible(true);
                lab3_modif.setVisible(true);
                lab4_modif.setVisible(true);
                lab5_modif.setVisible(true);
                lab6_modif.setVisible(true);

                
                String numero = champ1_modif.getText();
                String[] nom=new String [10];
                String[] prenom=new String [10];              
                String[] adresse=new String [10];
                String[] tel=new String [10];              
                String[] mutuelle=new String [10];
                int i = 0;
                
                try {
                    
                Statement state = connexion.getConn().createStatement();
                result = state.executeQuery("SELECT * from malade WHERE numero='"+numero+"'");
                
                
                while(result.next()){
                    nom[i] = result.getString("nom");
                    prenom[i] = result.getString("prenom");
                    adresse[i] = result.getString("adresse");
                    tel[i] = result.getString("tel");
                    mutuelle[i] = result.getString("mutuelle");
                    i++;  
                }
                } catch (SQLException ex) {
                Logger.getLogger(InterfaceMaJ.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                champ2_modif.setText(nom[0]);
                champ3_modif.setText(prenom[0]);
                champ4_modif.setText(adresse[0]);
                champ5_modif.setText(tel[0]);
                champ6_modif.setText(mutuelle[0]);
                
                
            }
            if(table3.equals("service")){     
                cle1=champ1_modif.getText();
                lab2_modif.setText("Nom : ");
                lab3_modif.setText("Batiment : ");
                lab4_modif.setText("Directeur : ");
                champ2_modif.setVisible(true);
                champ3_modif.setVisible(true);
                champ4_modif.setVisible(true);
                champ5_modif.setVisible(false);
                champ6_modif.setVisible(false);
                lab2_modif.setVisible(true);
                lab3_modif.setVisible(true);
                lab4_modif.setVisible(true);
                lab5_modif.setVisible(false);
                lab6_modif.setVisible(false);

                
                String code = champ1_modif.getText();
                String[] nom=new String [10];
                String[] batiment=new String [10];              
                String[] directeur=new String [10];
                int i = 0;
                
                try {
                    
                Statement state = connexion.getConn().createStatement();
                result = state.executeQuery("SELECT * from service WHERE code='"+code+"'");
                
                
                while(result.next()){
                    nom[i] = result.getString("nom");
                    batiment[i] = result.getString("batiment");
                    directeur[i] = result.getString("directeur");
                    i++;  
                }
                } catch (SQLException ex) {
                Logger.getLogger(InterfaceMaJ.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                champ2_modif.setText(nom[0]);
                champ3_modif.setText(batiment[0]);
                champ4_modif.setText(directeur[0]);
                
                
            }
            if(table3.equals("soigne")){
                cle1=champ1_modif.getText();
                cle2=champ2_modif.getText();
                champ3_modif.setVisible(false);
                champ4_modif.setVisible(false);
                champ5_modif.setVisible(false);
                champ6_modif.setVisible(false);
                lab3_modif.setVisible(false);
                lab4_modif.setVisible(false);
                lab5_modif.setVisible(false);
                lab6_modif.setVisible(false);

                
                String no_docteur = champ1_modif.getText();
                String no_malade = champ2_modif.getText();
                int i = 0;
                
                try {
                    
                Statement state = connexion.getConn().createStatement();
                result = state.executeQuery("SELECT * from soigne WHERE no_docteur='"+no_docteur+"'"+"AND no_malade ='"+no_malade+"'");
                
      
                } catch (SQLException ex) {
                Logger.getLogger(InterfaceMaJ.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        }
        if(source == valider3){
            String query = null;           
            if(table3.equals("chambre")){
                query = "UPDATE "+table3+" SET code_service = '"+champ1_modif.getText()+"',no_chambre = '"+champ2_modif.getText()+"',surveillant = '"+champ3_modif.getText()+"',nb_lits = '"+champ4_modif.getText()+"'  WHERE code_service = '"+cle1+"' AND no_chambre = '"+cle2+"' ";
            }
            if(table3.equals("docteur")){
                query = "UPDATE "+table3+" SET numero = '"+champ1_modif.getText()+"',specialite = '"+champ2_modif.getText()+"'  WHERE numero = '"+cle1+"' ";
            }
            if(table3.equals("employe")){
                query = "UPDATE "+table3+" SET numero = '"+champ1_modif.getText()+"',nom = '"+champ2_modif.getText()+"',prenom = '"+champ3_modif.getText()+"',adresse = '"+champ4_modif.getText()+"',tel = '"+champ5_modif.getText()+"'  WHERE numero = '"+cle1+"' ";
            }
            if(table3.equals("hospitalisation")){
                query = "UPDATE "+table3+" SET no_malade = '"+champ1_modif.getText()+"',code_service = '"+champ2_modif.getText()+"',no_chambre = '"+champ3_modif.getText()+"',lit = '"+champ4_modif.getText()+"' WHERE no_malade = '"+cle1+"' ";
            }
            if(table3.equals("infirmier")){
                query = "UPDATE "+table3+" SET numero = '"+champ1_modif.getText()+"',code_service = '"+champ2_modif.getText()+"',rotation = '"+champ3_modif.getText()+"',salaire = '"+champ4_modif.getText()+"' WHERE numero = '"+cle1+"' ";
            }
            if(table3.equals("malade")){
                query = "UPDATE "+table3+" SET numero = '"+champ1_modif.getText()+"',nom = '"+champ2_modif.getText()+"', prenom = '"+champ3_modif.getText()+"', adresse = '"+champ4_modif.getText()+"', tel = '"+champ5_modif.getText()+"', mutuelle = '"+champ6_modif.getText()+"' WHERE numero = '"+cle1+"' ";
                
            }
            if(table3.equals("service")){
                query = "UPDATE "+table3+" SET code = '"+champ1_modif.getText()+"',nom = '"+champ2_modif.getText()+"',batiment = '"+champ3_modif.getText()+"',directeur = '"+champ4_modif.getText()+"' WHERE code = '"+cle1+"' ";
            }
            if(table3.equals("soigne")){
                query = "UPDATE "+table3+" SET no_docteur = '"+champ1_modif.getText()+"',no_malade = '"+champ2_modif.getText()+"' WHERE no_docteur = '"+cle1+"' AND no_malade = '"+cle2+"' ";
            }
            maj.Modificate(connexion, query);
        }
        
    }
}

