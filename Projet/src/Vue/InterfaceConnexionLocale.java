/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vue;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;



/**
 *
 * @author Dany
 */
public class InterfaceConnexionLocale extends JDialog {
    
    
    
    private boolean sendData = false;
    private boolean close = false;
    private JLabel nameLabel, loginLabel, passwordLabel; 
    private JButton validate;
    private JTextField nom, logn, passwd ;
    
    public String N , L ;
    public String P = "";
    

    
  public InterfaceConnexionLocale(JFrame parent, String title, boolean modal){
      
   super(parent, title, modal);
    this.setSize(300, 500);
    this.setLocationRelativeTo(null);
   
    this.setResizable(false);
    
    

    
        JPanel panName = new JPanel();
        panName.setBackground(Color.white);
        panName.setPreferredSize(new Dimension(220, 60));
        nom = new JTextField();
        nom.setPreferredSize(new Dimension(100, 25));
        panName.setBorder(BorderFactory.createTitledBorder("Nom de la BDD"));
        nameLabel = new JLabel("Saisir un nom :");
        panName.add(nameLabel);
        panName.add(nom);
        
        JPanel panLogin = new JPanel();
        panLogin.setBackground(Color.white);
        panLogin.setPreferredSize(new Dimension(220, 60));
        logn = new JTextField();
        logn.setPreferredSize(new Dimension(100, 25));
        panLogin.setBorder(BorderFactory.createTitledBorder("Login"));
        loginLabel = new JLabel("Saisir un login :");
        panLogin.add(loginLabel);
        panLogin.add(logn);
        
        JPanel panPassword = new JPanel();
        panPassword.setBackground(Color.white);
        panPassword.setPreferredSize(new Dimension(220, 60));
        passwd = new JTextField();
        passwd.setPreferredSize(new Dimension(100, 25));
        panPassword.setBorder(BorderFactory.createTitledBorder("Mot de passe"));
        passwordLabel = new JLabel("Saisir un mdp :");
        panPassword.add(passwordLabel);
        panPassword.add(passwd);

        JPanel control = new JPanel();
        validate = new JButton("Se connecter");

        validate.addActionListener(new ActionListener(){
          public void actionPerformed(ActionEvent e) {        
            N = nom.getText();
            L = logn.getText();
            P = passwd.getText();
            sendData = true;
            //info = new Infos(nom.getText(), logn.getText(), passwd.getText());
            setVisible(false);
          }
            });
    
    
        JPanel content = new JPanel();
        content.setBackground(Color.white);
        content.add(panName);
        content.add(panLogin);
        content.add(panPassword);
        control.add(validate);
    
    
        this.getContentPane().add(content, BorderLayout.CENTER);
        this.getContentPane().add(control, BorderLayout.SOUTH);
        this.addWindowListener(new WindowAdapter() { 
    @Override public void windowClosed(WindowEvent e) { 
        close = true;
    }
  });
        this.setVisible(true);
    
    }
  public String getNom(){
      return N;
  }
  public String getLogin(){
      return L ; 
  }
  public String getPassword(){
      return P ; 
  }
  public boolean getSendData(){
      return sendData;
  }
  public boolean getClose(){
      return close;
  }
}
    
