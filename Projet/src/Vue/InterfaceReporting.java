/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vue;

import Controler.*;
import Modele.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;






/**
 *
 * @author Dany
 */
public class InterfaceReporting  extends JPanel implements ActionListener {
    
   
    
    
    Connexion connexion;
    Reporting reporting;
    
    JComboBox tableBox ; 
    
    JPanel reportingChambre; 
    JPanel reportingDocteur; 
    JPanel reportingEmploye; 
    JPanel reportingHospitalisation; 
    JPanel reportingInfirmier; 
    JPanel reportingMalade; 
    JPanel reportingService; 
    JPanel reportingSoigne; 
    
    JButton nb_lits;
    JButton tauxoqp;
    JButton soigneUn;
    JButton soignePas;
    
    JButton employeD;
    JButton employeI;
    
    JButton mutuelle; 
    JButton salaireInfirmier;
    JButton infirmierNuit;
    JButton repartitionS;
    
    JButton rapport;
    JButton nbSoigne;
    JButton hospitalisation;
    
    public InterfaceReporting(Connexion c){
        this.setLayout(null);
        connexion = c; 
        
        reporting = new Reporting(connexion);
        
        String[] tab = {"chambre", "docteur", "employe", "hospitalisation", "infirmier", "malade", "service", "soigne"};
        
        JPanel titre = new JPanel();
        titre.setLayout(new BorderLayout());
        titre.setBackground(new Color(93, 173, 226));
        
        
        JLabel textetitre = new JLabel("Module Reporting"); 
        Font f1 = textetitre.getFont().deriveFont(22.0f); 
        textetitre.setFont(f1);
        
        titre.add(textetitre, BorderLayout.CENTER);
        
        titre.setBounds(0, 0, 750, 50);
        this.add(titre);
        
        
        JPanel InfoTable = new JPanel();
        InfoTable.setBackground(Color.white);
        tableBox = new JComboBox(tab);
        tableBox.setPreferredSize(new Dimension(200, 20));
        InfoTable.setBorder(BorderFactory.createTitledBorder("De quelle table voulez-vous les informations?"));
        tableBox.addActionListener(this);
        InfoTable.add(tableBox);
        InfoTable.setBounds(0,50, 800, 149);
       
        this.add(InfoTable);
        
        
        reportingChambre = new JPanel(); 
        
        reportingChambre.setBackground(Color.white);
        reportingChambre.setBounds(0, 200, 750, 750);
        reportingChambre.setLayout(null);
        
        nb_lits = new JButton("Informations lits");
        nb_lits.setBounds(0,0,200,50);
        reportingChambre.add(nb_lits);
        nb_lits.addActionListener(new BoutonListener());
        reportingChambre.setVisible(true);
        this.add(reportingChambre);
        
        tauxoqp = new JButton("Taux d'occupation");
        tauxoqp.setBounds(0,100,200,50);
        reportingChambre.add(tauxoqp);
        tauxoqp.addActionListener(new BoutonListener());
        reportingChambre.setVisible(true);
        this.add(reportingChambre);
        
        
        reportingDocteur = new JPanel(); 
        
        reportingDocteur.setBackground(Color.white);
        reportingDocteur.setBounds(0, 200, 750, 750);
        reportingDocteur.setLayout(null);
        
        soigneUn = new JButton("Docteurs ayant au moins un malade hospitalisé");
        soigneUn.setBounds(0,0,500,50);
        reportingDocteur.add(soigneUn);
        soigneUn.addActionListener(new BoutonListener());
        reportingDocteur.setVisible(false);
        this.add(reportingDocteur);
        
        soignePas = new JButton("Docteurs n'ayant aucun malade hospitalisé");
        soignePas.setBounds(0,100,500,50);
        reportingDocteur.add(soignePas);
        soignePas.addActionListener(new BoutonListener());
        reportingDocteur.setVisible(false);
        this.add(reportingDocteur);
        
        
        repartitionS = new JButton("Répartition des spécialités");
        repartitionS.setBounds(0,200,500,50);
        reportingDocteur.add(repartitionS);
        repartitionS.addActionListener(new BoutonListener());
        reportingDocteur.setVisible(false);
        this.add(reportingDocteur);
        
        
        reportingEmploye = new JPanel(); 
        
        reportingEmploye.setBackground(Color.white);
        reportingEmploye.setBounds(0, 200, 750, 750);
        reportingEmploye.setLayout(null);
        reportingEmploye.setVisible(false);
        this.add(reportingEmploye);
        
        // Bouton taux docteurs
        employeD = new JButton("Docteurs");
        employeD.setBounds(0,0,500,50);
        reportingEmploye.add(employeD);
        employeD.addActionListener(new BoutonListener());
        reportingEmploye.setVisible(false);
        this.add(reportingEmploye);
        
        employeI = new JButton("Infirmiers");
        employeI.setBounds(0,100,500,50);
        reportingEmploye.add(employeI);
        employeI.addActionListener(new BoutonListener());
        reportingEmploye.setVisible(false);
        this.add(reportingEmploye);
        
        
        reportingHospitalisation = new JPanel(); 
        
        reportingHospitalisation.setBackground(Color.white);
        reportingHospitalisation.setBounds(0, 200, 750, 750);
        reportingHospitalisation.setLayout(null);
        reportingHospitalisation.setVisible(false);
        this.add(reportingHospitalisation);
        
        //bouton nombre d'hospitalisés par service
        hospitalisation = new JButton("Nombre d'hospitalisés par service");
        hospitalisation.setBounds(0,0,500,50);
        reportingHospitalisation.add(hospitalisation);
        hospitalisation.addActionListener(new BoutonListener());
        reportingHospitalisation.setVisible(false);
        this.add(reportingHospitalisation);
        
        
        
        
        reportingInfirmier = new JPanel(); 
        
        reportingInfirmier.setBackground(Color.white);
        reportingInfirmier.setBounds(0, 200, 750, 750);
        reportingInfirmier.setLayout(null);
        reportingInfirmier.setVisible(false);
        this.add(reportingInfirmier);
        
        //bouton salaire moyen des infirmiers
        salaireInfirmier = new JButton("Salaire moyen des infirmiers par Service");
        salaireInfirmier.setBounds(0,0,500,50);
        reportingInfirmier.add(salaireInfirmier);
        salaireInfirmier.addActionListener(new BoutonListener());
        reportingInfirmier.setVisible(false);
        this.add(reportingInfirmier);
        
        //bouton infirmier travaillant pendant la rotation de nuit
        infirmierNuit = new JButton("Infirmiers travaillant pendant la rotation de NUIT");
        infirmierNuit.setBounds(0,100,500,50);
        reportingInfirmier.add(infirmierNuit);
        infirmierNuit.addActionListener(new BoutonListener());
        reportingInfirmier.setVisible(false);
        this.add(reportingInfirmier);
        
        
        
        
        
        
        reportingMalade = new JPanel(); 
        
        reportingMalade.setBackground(Color.white);
        reportingMalade.setBounds(0, 200, 750, 750);
        reportingMalade.setLayout(null);
        reportingMalade.setVisible(false);
        this.add(reportingMalade);
        
        // Bouton relatif à la répartition des mutuelles des malades
        mutuelle = new JButton("Répartition des mutuelles");
        mutuelle.setBounds(0,0,500,50);
        reportingMalade.add(mutuelle);
        mutuelle.addActionListener(new BoutonListener());
        reportingMalade.setVisible(false);
        this.add(reportingMalade);
        
        
        
        
        reportingService = new JPanel(); 
        
        reportingService.setBackground(Color.white);
        reportingService.setBounds(0, 200, 750, 750);
        reportingService.setLayout(null);
        reportingService.setVisible(false);
        this.add(reportingService);
        
        // Bouton relatif au rapport nb d'infirmiers par malades
        rapport = new JButton("Rapport nombre d’infirmier sur le nombre de malades hospitalisés par service");
        rapport.setBounds(0,0,500,50);
        reportingService.add(rapport);
        rapport.addActionListener(new BoutonListener());
        reportingService.setVisible(false);
        this.add(reportingService);
        
        
        
        reportingSoigne = new JPanel(); 
        
        reportingSoigne.setBackground(Color.white);
        reportingSoigne.setBounds(0, 200, 750, 750);
        reportingSoigne.setLayout(null);
        reportingSoigne.setVisible(false);
        this.add(reportingSoigne);
        
        // Bouton relatif au nb de malades par médecin
        nbSoigne = new JButton("Nombre de malades que soigne chaque médecin");
        nbSoigne.setBounds(0,0,500,50);
        reportingSoigne.add(nbSoigne);
        nbSoigne.addActionListener(new BoutonListener());
        reportingSoigne.setVisible(false);
        this.add(reportingSoigne);
        
        
        
        
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==tableBox){
            System.out.println(tableBox.getSelectedItem());
            if(tableBox.getSelectedItem().equals("chambre"))
            {
                reportingDocteur.setVisible(false);
                reportingEmploye.setVisible(false);
                reportingHospitalisation.setVisible(false);
                reportingInfirmier.setVisible(false);
                reportingMalade.setVisible(false);
                reportingService.setVisible(false);
                reportingSoigne.setVisible(false);
                reportingChambre.setVisible(true);
            }
            if(tableBox.getSelectedItem().equals("docteur")){
                reportingChambre.setVisible(false);
                reportingDocteur.setVisible(true);
                reportingEmploye.setVisible(false);
                reportingHospitalisation.setVisible(false);
                reportingInfirmier.setVisible(false);
                reportingMalade.setVisible(false);
                reportingService.setVisible(false);
                reportingSoigne.setVisible(false);
            }
            if(tableBox.getSelectedItem().equals("employe")){
                reportingChambre.setVisible(false);
                reportingDocteur.setVisible(false);
                reportingEmploye.setVisible(true);
                reportingHospitalisation.setVisible(false);
                reportingInfirmier.setVisible(false);
                reportingMalade.setVisible(false);
                reportingService.setVisible(false);
                reportingSoigne.setVisible(false);
            }
            if(tableBox.getSelectedItem().equals("hospitalisation")){
                reportingChambre.setVisible(false);
                reportingDocteur.setVisible(false);
                reportingEmploye.setVisible(false);
                reportingHospitalisation.setVisible(true);
                reportingInfirmier.setVisible(false);
                reportingMalade.setVisible(false);
                reportingService.setVisible(false);
                reportingSoigne.setVisible(false);
            }
            if(tableBox.getSelectedItem().equals("infirmier")){
                reportingChambre.setVisible(false);
                reportingDocteur.setVisible(false);
                reportingEmploye.setVisible(false);
                reportingHospitalisation.setVisible(false);
                reportingInfirmier.setVisible(true);
                reportingMalade.setVisible(false);
                reportingService.setVisible(false);
                reportingSoigne.setVisible(false);
            }
            if(tableBox.getSelectedItem().equals("malade")){
                reportingChambre.setVisible(false);
                reportingDocteur.setVisible(false);
                reportingEmploye.setVisible(false);
                reportingHospitalisation.setVisible(false);
                reportingInfirmier.setVisible(false);
                reportingMalade.setVisible(true);
                reportingService.setVisible(false);
                reportingSoigne.setVisible(false);
            }
            if(tableBox.getSelectedItem().equals("service")){
                reportingChambre.setVisible(false);
                reportingDocteur.setVisible(false);
                reportingEmploye.setVisible(false);
                reportingHospitalisation.setVisible(false);
                reportingInfirmier.setVisible(false);
                reportingMalade.setVisible(false);
                reportingService.setVisible(true);
                reportingSoigne.setVisible(false);
            }
            if(tableBox.getSelectedItem().equals("soigne")){
                reportingChambre.setVisible(false);
                reportingDocteur.setVisible(false);
                reportingEmploye.setVisible(false);
                reportingHospitalisation.setVisible(false);
                reportingInfirmier.setVisible(false);
                reportingMalade.setVisible(false);
                reportingService.setVisible(false);
                reportingSoigne.setVisible(true);
            }
        }
    }
    
    class BoutonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if(e.getSource()==nb_lits){
                String reportingchambre ="nblits";
                ResultSet r = reporting.reportingChambre(reportingchambre);
                InterfaceDonnees id = new InterfaceDonnees(); 
                id.InfoChambresLits(r);
               
            }
            if(e.getSource()==tauxoqp){
                String reportingchambre ="tauxoccupation";
                ResultSet r = reporting.reportingChambre(reportingchambre);
                InterfaceDonnees id = new InterfaceDonnees(); 
                id.InfoTauxOccupation(r);
               
            }
            if(e.getSource()==soigneUn){
                
                ResultSet r = reporting.DocteurAyantAuMoinsUn();
                InterfaceDonnees id = new InterfaceDonnees(); 
                id.Docteur(r);
               
                
            }
            if(e.getSource()==soignePas){
                
                ResultSet r = reporting.DocteurPas();
                InterfaceDonnees id = new InterfaceDonnees(); 
                id.Docteur(r);
               
                
            }
            
            if(e.getSource()==mutuelle){
                
                ResultSet r = reporting.mutuelle();
                InterfaceDonnees id = new InterfaceDonnees(); 
                id.mutuelle(r);
               
                
            }
            
            if(e.getSource()==salaireInfirmier){
                
                ResultSet r = reporting.salaireMoyen();
                InterfaceDonnees id = new InterfaceDonnees(); 
                id.salaireInfirmier(r);
               
                
            }
            if(e.getSource()==infirmierNuit){
                ResultSet r = reporting.infirmierNuit();
                InterfaceDonnees id = new InterfaceDonnees(); 
                id.infirmierNuit(r);
               
            }
            if(e.getSource()==rapport){
                ResultSet r = reporting.rapport();
                InterfaceDonnees id = new InterfaceDonnees(); 
                id.rapport(r);
               
            }
            
            if(e.getSource()==employeD){
                ResultSet r = reporting.employeD();
                InterfaceDonnees id = new InterfaceDonnees(); 
                id.employeD(r);
               
            }
            
            if(e.getSource()==employeI){
                ResultSet r = reporting.employeI();
                InterfaceDonnees id = new InterfaceDonnees(); 
                id.employeI(r);
               
            }
            
            if(e.getSource()==repartitionS){
                ResultSet r = reporting.repartitionS();
                InterfaceDonnees id = new InterfaceDonnees(); 
                id.repartitionS(r);
               
            }
            
            if(e.getSource()==nbSoigne){
                ResultSet r = reporting.nbSoigne();
                InterfaceDonnees id = new InterfaceDonnees(); 
                id.nbSoigne(r);
               
            }
            
            if(e.getSource()==hospitalisation){
                ResultSet r = reporting.hospitalisation();
                InterfaceDonnees id = new InterfaceDonnees(); 
                id.hospitalisation(r);
               
            }
            
        }
        
        }

}
