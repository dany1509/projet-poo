/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vue;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;



/**
 *
 * @author Dany
 */
public class InterfaceConnexionDistante extends JDialog {
    
    
    
    private boolean sendData=false;
    private boolean close=false;
    private JLabel loginECELabel, passwordECELabel, loginDatabaseLabel, passwordDatabaseLabel; 
    private JButton validate;
    private JTextField loginECEText, passwordECEText, loginDatabaseText, passwordDatabaseText ;
    
    private String usernameECE, passwordECE, loginDatabase, passwordDatabase;
    

    
  public InterfaceConnexionDistante(JFrame parent, String title, boolean modal){
      
    super(parent, title, modal);
    this.setSize(400, 500);
    this.setLocationRelativeTo(null);
    this.setTitle(title);
    
    this.setResizable(false);
    
    
    //this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    

    
        JPanel panName = new JPanel();
        panName.setBackground(Color.white);
        panName.setPreferredSize(new Dimension(220, 60));
        loginECEText = new JTextField();
        loginECEText.setPreferredSize(new Dimension(100, 25));
        panName.setBorder(BorderFactory.createTitledBorder("Login ECE"));
        loginECELabel = new JLabel("Saisir un login :");
        panName.add(loginECELabel);
        panName.add(loginECEText);
        
        JPanel panLogin = new JPanel();
        panLogin.setBackground(Color.white);
        panLogin.setPreferredSize(new Dimension(220, 60));
        passwordECEText = new JTextField();
        passwordECEText.setPreferredSize(new Dimension(100, 25));
        panLogin.setBorder(BorderFactory.createTitledBorder("Mdp ECE"));
        passwordECELabel = new JLabel("Saisir un mdp :");
        panLogin.add(passwordECELabel);
        panLogin.add(passwordECEText);
        
        JPanel panPassword = new JPanel();
        panPassword.setBackground(Color.white);
        panPassword.setPreferredSize(new Dimension(220, 60));
        loginDatabaseText = new JTextField();
        loginDatabaseText.setPreferredSize(new Dimension(100, 25));
        panPassword.setBorder(BorderFactory.createTitledBorder("Login BDD"));
        loginDatabaseLabel = new JLabel("Saisir le login :");
        panPassword.add(loginDatabaseLabel);
        panPassword.add(loginDatabaseText);
        
        JPanel panPassword2 = new JPanel();
        panPassword2.setBackground(Color.white);
        panPassword2.setPreferredSize(new Dimension(220, 60));
        passwordDatabaseText = new JTextField();
        passwordDatabaseText.setPreferredSize(new Dimension(100, 25));
        panPassword2.setBorder(BorderFactory.createTitledBorder("Mdp BDD"));
        passwordDatabaseLabel = new JLabel("Saisir le mdp :");
        panPassword2.add(passwordDatabaseLabel);
        panPassword2.add(passwordDatabaseText);

        JPanel control = new JPanel();
        validate = new JButton("Se connecter");

        validate.addActionListener(new ActionListener(){
          public void actionPerformed(ActionEvent e) {        
            usernameECE = loginECEText.getText();
            passwordECE = passwordECEText.getText();
            loginDatabase = loginDatabaseText.getText();
            passwordDatabase = passwordDatabaseText.getText();
            sendData = true;
            //info = new Infos(nom.getText(), logn.getText(), passwd.getText());
            setVisible(false);
          }
            });
    
    
        JPanel content = new JPanel();
        content.setBackground(Color.white);
        content.add(panName);
        content.add(panLogin);
        content.add(panPassword);
        content.add(panPassword2);
        control.add(validate);
    
    
        this.getContentPane().add(content, BorderLayout.CENTER);
        this.getContentPane().add(control, BorderLayout.SOUTH);
        this.setVisible(true);
        
        
        this.addWindowListener(new WindowAdapter() { 
    @Override public void windowClosed(WindowEvent e) { 
        close = true;
    }
  });
    }
    
   
  public String getUsernameECE(){
      return usernameECE;
  }
  public String getPasswordECE(){
      return passwordECE ; 
  }
  public String getLoginDatabase(){
      return loginDatabase ; 
  }
  public String getPasswordDatabase(){
      return passwordDatabase ; 
  }
  public boolean getSendData(){
      return sendData;
  }
  
  public boolean getClose(){
      return close;
  }
}
    
