/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vue;

import Modele.*;
import Controler.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author Dany
 */
public class Fenetre extends JFrame {
    
    public Connexion c; 
    private JPanel paneldroit;
    public Menu m ; 
    private Accueil a; 
    private InterfaceRecherche interface_recherche = new InterfaceRecherche(c);
    
    private JSplitPane split;
    
    public Fenetre(Connexion co){
        c = co; 
        //this.setLocationRelativeTo(null);
        this.setTitle("Gestion de la base de données");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(1000, 1000);
        this.setResizable(false);
        
        m = new Menu(this, c) ;
        a = new Accueil();
        
        paneldroit = new JPanel();
        paneldroit.setSize(new Dimension(750,800));
        
        split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, m, a);
        
        this.getContentPane().add(split, BorderLayout.CENTER);
        this.setVisible(true);
        
    }
    
    public void updateFenetre(JPanel p){
        paneldroit = p;
        split.setRightComponent(paneldroit);
        repaint();
        /*split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, m, paneldroit);
        
        this.getContentPane().add(split, BorderLayout.CENTER);
        this.setVisible(true);*/
    }
    
    public void setPanelDroit(JPanel p){
        paneldroit = p;
    }
    
}