/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vue;


import Controler.*;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;

/**
 *
 * @author Dany
 */
public class InterfaceResultats extends JFrame {
    
    private JPanel result = new JPanel();
    private JComboBox combo = new JComboBox();
    private JTextField jtf = new JTextField("");
    private JButton b = new JButton("Valider");
    private JLabel res = new JLabel(); 
    
    /**
    * Le délimiteur
    */
    private JSplitPane split;
    

    public InterfaceResultats(){
        
        this.setTitle("Resultats");
        this.setSize(900, 600);

        this.setLocationRelativeTo(null);
        initContent();

        this.setContentPane(result);
        this.setVisible(true);
        this.addWindowListener(new WindowAdapter()
        {
        public void windowClosing(WindowEvent e)
        {
            System.out.println("Window Closing");
        }
        });
        
        
        
        
        
    }
    
    /**
    * Initialise le contenu de la fenêtre
    */
    public void initContent(){
      //Vous connaissez ça...
      result.setLayout(new BorderLayout());
      split = new JSplitPane(JSplitPane.VERTICAL_SPLIT, new JScrollPane(b), result);
      split.setDividerLocation(100);
      getContentPane().add(split, BorderLayout.CENTER);		
    }
    
  public void initTable(ResultSet res){
    try {
      
      //On récupère les MetaData
      ResultSetMetaData meta = res.getMetaData();
      
      //On initialise un tableau d'Object pour les en-têtes du tableau
      Object[] column = new Object[meta.getColumnCount()];

      for(int i = 1 ; i <= meta.getColumnCount(); i++)
        column[i-1] = meta.getColumnName(i);

      //Petite manipulation pour obtenir le nombre de lignes
      res.last();
      int rowCount = res.getRow();
      Object[][] data = new Object[res.getRow()][meta.getColumnCount()];

      //On revient au départ
      res.beforeFirst();
      int j = 1;

      //On remplit le tableau d'Object[][]
      while(res.next()){
        for(int i = 1 ; i <= meta.getColumnCount(); i++)
          data[j-1][i-1] = res.getObject(i);
				
        j++;
      }

      //On ferme le tout                                     
      res.close();

      //On enlève le contenu de notre conteneur
      result.removeAll();
      //On y ajoute un JTable
      result.add(new JScrollPane(new JTable(data, column)), BorderLayout.CENTER);
      //On force la mise à jour de l'affichage
      result.revalidate();
			
    } catch (SQLException e) {
      //Dans le cas d'une exception, on affiche une pop-up et on efface le contenu		
      result.removeAll();
      result.add(new JScrollPane(new JTable()), BorderLayout.CENTER);
      result.revalidate();
      JOptionPane.showMessageDialog(null, e.getMessage(), "ERREUR ! ", JOptionPane.ERROR_MESSAGE);
    }	
  }
    
    class BoutonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
        
            res.setText("Résultat: " + jtf.getText() + " avec " + combo.getSelectedItem());
        
        }
    }
    
}
