/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vue;

import Controler.*;
import Modele.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Julien
 */
public class InterfaceRecherche extends JPanel implements ActionListener {

    private JTextField champ = new JTextField();
    private JComboBox tableBox = new JComboBox();
    private JComboBox tableBox2 = new JComboBox();
    private JComboBox tableBox3 = new JComboBox();
    private JComboBox critere = new JComboBox();
    private JComboBox critere2 = new JComboBox();
    private JComboBox critere3 = new JComboBox();
    private JCheckBox startWith = new JCheckBox("Commençant par ...");
    private JPanel Recherche = new JPanel();
    private JPanel checkbox = new JPanel();
    private JButton valider = new JButton("Valider les choix");
    private JButton validerR = new JButton("Lancer la recherche");

    private String table, table2, table3, field, crit, crit2, crit3;
    private String colonneSelect = " * ";
    private List<String> checkArray = new ArrayList<String>();
    private String[] ColumnName;
    JCheckBox[] j = new JCheckBox[0];
    private boolean RechercheAvancee;
    private boolean isValider = false;
    private boolean checkBoxAdded = false;

    private ResultSet r;

    Connexion connexion;

    @SuppressWarnings("unchecked")

    public InterfaceRecherche(Connexion c) {
        connexion = c;

        //this.setSize(700, 700);
        String[] tab = {"chambre", "docteur", "employe", "hospitalisation", "infirmier", "malade", "service", "soigne"};
        String[] tab2 = {"", "chambre", "docteur", "employe", "hospitalisation", "infirmier", "malade", "service", "soigne"};
        String[] tabCritere = {"", "code_service", "no_chambre", "surveillant", "nb_lits"};
        String[] tabCritere2 = {""};

        JPanel InfoTable = new JPanel();
        InfoTable.setBackground(Color.white);
        InfoTable.setPreferredSize(new Dimension(400, 60));
        tableBox = new JComboBox(tab);
        tableBox.setPreferredSize(new Dimension(250, 20));
        InfoTable.setBorder(BorderFactory.createTitledBorder("Dans quelle table voulez-vous faire la recherche"));
        tableBox.addActionListener(this);
        InfoTable.add(tableBox);

        JPanel InfoR = new JPanel();
        InfoR.setBackground(Color.white);
        InfoR.setPreferredSize(new Dimension(400, 100));
        champ.setPreferredSize(new Dimension(150, 27));
        critere = new JComboBox(tabCritere);
        InfoR.setBorder(BorderFactory.createTitledBorder("Recherche"));
        InfoR.add(champ);
        InfoR.add(critere);
        InfoR.add(startWith);

        JPanel InfoRA = new JPanel();
        JPanel InfoRA_1 = new JPanel();
        JLabel lb1 = new JLabel();
        JLabel lb2 = new JLabel();

        // Panel pour la recherche avancée avec deux tables
        JPanel RA_1 = new JPanel();
        JPanel RA_2 = new JPanel();
        RA_1.setLayout(new BoxLayout(RA_1, BoxLayout.LINE_AXIS));
        RA_1.setBackground(Color.white);
        RA_2.setBackground(Color.white);
        tableBox2 = new JComboBox(tab2);
        tableBox2.setPreferredSize(new Dimension(250, 20));
        tableBox2.addActionListener(this);
        RA_1.add(tableBox2);
        InfoRA.setBackground(Color.white);
        InfoRA_1.setBackground(Color.white);
        InfoRA.setPreferredSize(new Dimension(700, 300));
        critere2 = new JComboBox(tabCritere2);
        critere2.setPreferredSize(new Dimension(250 / 2, 20));
        lb1.setText("Recherche avec deux tables : ");
        RA_1.add(Box.createRigidArea(new Dimension(10, 0)));
        RA_1.add(critere2);

        //JPanel pour recherche avancée avec trois tables
        JPanel RA_3 = new JPanel();
        JPanel RA_4 = new JPanel();
        RA_3.setBackground(Color.white);
        RA_4.setBackground(Color.white);
        RA_3.setLayout(new BoxLayout(RA_3, BoxLayout.LINE_AXIS));
        tableBox3 = new JComboBox(tab2);
        tableBox3.setPreferredSize(new Dimension(250, 20));
        tableBox3.addActionListener(this);
        RA_3.add(tableBox3);
        critere3 = new JComboBox(tabCritere2);
        critere3.setPreferredSize(new Dimension(250 / 2, 20));
        lb1.setText("Recherche avec deux tables : ");
        lb2.setText("Recherche avec trois tables : ");
        RA_3.add(Box.createRigidArea(new Dimension(10, 0)));
        RA_3.add(critere3);

        // On ajoute tout à un JPanel qui va contenir les comboBox et textField
        InfoRA.setBorder(BorderFactory.createTitledBorder("Recherche Avancée (jointures)"));
        InfoRA_1.setLayout(new BoxLayout(InfoRA_1, BoxLayout.PAGE_AXIS));
        InfoRA_1.add(lb1);
        InfoRA_1.add(Box.createRigidArea(new Dimension(0, 20)));
        InfoRA_1.add(RA_1);
        InfoRA_1.add(Box.createRigidArea(new Dimension(0, 15)));
        InfoRA_1.add(RA_2);
        InfoRA_1.add(Box.createRigidArea(new Dimension(0, 35)));
        InfoRA_1.add(lb2);
        InfoRA_1.add(Box.createRigidArea(new Dimension(0, 20)));
        InfoRA_1.add(RA_3);
        InfoRA_1.add(Box.createRigidArea(new Dimension(0, 15)));
        InfoRA_1.add(RA_4);

        // On ajoute au JPanel recherche avancée
        InfoRA.add(InfoRA_1);

        JPanel Valid = new JPanel();
        Valid.setBackground(Color.white);
        valider.addActionListener(new BoutonListener());
        validerR.addActionListener(new BoutonListener());
        Valid.add(valider);
        Valid.add(validerR);

        JPanel Infos = new JPanel();
        Infos.setPreferredSize(new Dimension(700, 50));
        JLabel aff = new JLabel("Laisser tous les champs vides pour afficher toutes les informations de la table");
        Infos.add(aff);

        checkbox.setBackground(Color.white);

        Recherche.setBackground(Color.white);
        Recherche.add(Infos);
        Recherche.add(InfoTable);
        Recherche.add(InfoR);
        Recherche.add(InfoRA);
        Recherche.add(checkbox);

        this.setLayout(new BorderLayout());
        this.add(Recherche, BorderLayout.CENTER);
        this.add(Valid, BorderLayout.SOUTH);
        this.setVisible(true);

    }

    public void updateR(String[] a, Object ComboUsed) {

        if (ComboUsed == tableBox) {

            critere.removeAllItems();

            for (int i = 0; i < a.length; i++) {
                critere.addItem(a[i]);
            }
        } else if (ComboUsed == tableBox2) {

            critere2.removeAllItems();

            for (int i = 0; i < a.length; i++) {
                critere2.addItem(a[i]);
            }
        } else if (ComboUsed == tableBox3) {

            critere3.removeAllItems();

            for (int i = 0; i < a.length; i++) {
                critere3.addItem(a[i]);

            }
        }
        repaint();
    }

    /**
     * @return the table
     */
    public String getTable() {
        return table;
    }

    /**
     * @return the field
     */
    public String getField() {
        return field;
    }

    /**
     * @return the crit
     */
    public String getCrit() {
        return crit;
    }

    /**
     * @return the table2
     */
    public String getTable2() {
        return table2;
    }


    /**
     * @return the crit2
     */
    public String getCrit2() {
        return crit2;
    }

    /**
     * @return the table3
     */
    public String getTable3() {
        return table3;
    }

    /**
     * @return the crit3
     */
    public String getCrit3() {
        return crit3;
    }
    


    /**
     * @return the RechercheAvancee
     */
    public boolean isRechercheAvancee() {
        return RechercheAvancee;
    }

    /**
     * @param RechercheAvancee the RechercheAvancee to set
     */
    public void setRechercheAvancee(boolean RechercheAvancee) {
        this.RechercheAvancee = RechercheAvancee;
    }

    public void actionPerformed(ActionEvent e) {
        String[] t = null;
        System.out.println("ActionListener : action sur " + tableBox.getSelectedItem());
        Object source = e.getSource();
        checkBoxAdded = false;
        checkArray.clear();
        colonneSelect = " * ";
        for (int i = 0; i < j.length; i++) {

            if (j[i].isSelected()) {
                j[i].setSelected(false);
            }
        }
        checkbox.removeAll();

        if (source == tableBox) {
            if (tableBox.getSelectedItem().equals("chambre")) {
                String[] a = {"", "code_service", "no_chambre", "surveillant", "nb_lits"};
                t = a;
            }
            if (tableBox.getSelectedItem().equals("docteur")) {
                String[] a = {"", "numero", "specialite"};
                t = a;
            }
            if (tableBox.getSelectedItem().equals("employe")) {
                String[] a = {"", "numero", "nom", "prenom", "adresse", "tel"};
                t = a;
            }
            if (tableBox.getSelectedItem().equals("hospitalisation")) {
                String[] a = {"", "no_malade", "code_service", "no_chambre", "lit"};
                t = a;
            }
            if (tableBox.getSelectedItem().equals("infirmier")) {
                String[] a = {"", "numero", "code_service", "rotation", "salaire"};
                t = a;
            }
            if (tableBox.getSelectedItem().equals("malade")) {
                String[] a = {"", "numero", "nom", "prenom", "adresse", "tel", "mutuelle"};
                t = a;
            }
            if (tableBox.getSelectedItem().equals("service")) {
                String[] a = {"", "code", "nom", "batiment", "directeur"};
                t = a;
            }
            if (tableBox.getSelectedItem().equals("soigne")) {
                String[] a = {"", "no_docteur", "no_malade"};
                t = a;
            }
        } else if (source == tableBox2) {
            if (tableBox2.getSelectedItem().equals("")) {
                String[] a = {""};
                t = a;
            }
            if (tableBox2.getSelectedItem().equals("chambre")) {
                String[] a = {"", "code_service", "no_chambre", "surveillant", "nb_lits"};
                t = a;
            }
            if (tableBox2.getSelectedItem().equals("docteur")) {
                String[] a = {"", "numero", "specialite"};
                t = a;
            }
            if (tableBox2.getSelectedItem().equals("employe")) {
                String[] a = {"", "numero", "nom", "prenom", "adresse", "tel"};
                t = a;
            }
            if (tableBox2.getSelectedItem().equals("hospitalisation")) {
                String[] a = {"", "no_malade", "code_service", "no_chambre", "lit"};
                t = a;
            }
            if (tableBox2.getSelectedItem().equals("infirmier")) {
                String[] a = {"", "numero", "code_service", "rotation", "salaire"};
                t = a;
            }
            if (tableBox2.getSelectedItem().equals("malade")) {
                String[] a = {"", "numero", "nom", "prenom", "adresse", "tel", "mutuelle"};
                t = a;
            }
            if (tableBox2.getSelectedItem().equals("service")) {
                String[] a = {"", "code", "nom", "batiment", "directeur"};
                t = a;
            }
            if (tableBox2.getSelectedItem().equals("soigne")) {
                String[] a = {"", "no_docteur", "no_malade"};
                t = a;
            }
        } else if (source == tableBox3) {
            if (tableBox3.getSelectedItem().equals("")) {
                String[] a = {""};
                t = a;
            }
            if (tableBox3.getSelectedItem().equals("chambre")) {
                String[] a = {"", "code_service", "no_chambre", "surveillant", "nb_lits"};
                t = a;
            }
            if (tableBox3.getSelectedItem().equals("docteur")) {
                String[] a = {"", "numero", "specialite"};
                t = a;
            }
            if (tableBox3.getSelectedItem().equals("employe")) {
                String[] a = {"", "numero", "nom", "prenom", "adresse", "tel"};
                t = a;
            }
            if (tableBox3.getSelectedItem().equals("hospitalisation")) {
                String[] a = {"", "no_malade", "code_service", "no_chambre", "lit"};
                t = a;
            }
            if (tableBox3.getSelectedItem().equals("infirmier")) {
                String[] a = {"", "numero", "code_service", "rotation", "salaire"};
                t = a;
            }
            if (tableBox3.getSelectedItem().equals("malade")) {
                String[] a = {"", "numero", "nom", "prenom", "adresse", "tel", "mutuelle"};
                t = a;
            }
            if (tableBox3.getSelectedItem().equals("service")) {
                String[] a = {"", "code", "nom", "batiment", "directeur"};
                t = a;
            }
            if (tableBox3.getSelectedItem().equals("soigne")) {
                String[] a = {"", "no_docteur", "no_malade"};
                t = a;
            }
        }

        this.updateR(t, source);
    }

    public void AjouterCheckbox(String[] columnName) {

        checkbox.removeAll();
        JPanel boxCheckBox1 = new JPanel();
        JPanel boxCheckBox2 = new JPanel();
        boxCheckBox1.setBackground(Color.white);
        boxCheckBox2.setBackground(Color.white);
        boxCheckBox1.setLayout(new BoxLayout(boxCheckBox1, BoxLayout.Y_AXIS));
        boxCheckBox1.add(Box.createRigidArea(new Dimension(0, 10)));
        boxCheckBox1.add(new JLabel("Quels champs voulez-vous afficher dans le résultat de la recherche ? :"));
        boxCheckBox1.add(Box.createRigidArea(new Dimension(0, 15)));
        j = new JCheckBox[columnName.length];
        boxCheckBox2.setLayout(new BoxLayout(boxCheckBox2, BoxLayout.Y_AXIS));
        for (int i = 0; i < columnName.length; i++) {
            j[i] = new JCheckBox(columnName[i]);
            j[i].addActionListener(new StateListener());
            j[i].setBackground(Color.white);
            boxCheckBox2.add(j[i]);

        }
        System.out.println(j.length);

        for (int i = 0; i < critere.getItemCount(); i++) {
            if (columnName[i] == "tout") {
                j[i].setText(columnName[i]);
            } else {
                j[i].setText(tableBox.getSelectedItem() + "." + columnName[i]);
            }
        }
        for (int i = 0; i <(critere.getItemCount() + critere2.getItemCount()) - 1; i++) {
            
            if (i >= critere.getItemCount()) {
                j[i].setText(tableBox2.getSelectedItem() + "." + columnName[i]);
            }
        }
        for (int i = 0; i < (critere.getItemCount() + critere2.getItemCount() + critere3.getItemCount()) - 2; i++) {
            if (i >= (critere.getItemCount() + critere2.getItemCount()) - 1) {
                j[i].setText(tableBox3.getSelectedItem() + "." + columnName[i]);
            }
        }

        for(int i=0; i<j.length;i++)
            System.out.println(j[i].getText());
        checkbox.setLayout(new BoxLayout(checkbox, BoxLayout.PAGE_AXIS));
        checkbox.add(boxCheckBox1);
        checkbox.add(boxCheckBox2);
        validate();
        repaint();
        setVisible(true);

    }

    class StateListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            Object source = e.getSource();
            int cpt = 0;

            //Itérateur de liste chechArray
            Iterator itr = checkArray.iterator();

            // Si une checkbox est sélectionnée on ajoute son nom à la liste
            if (((JCheckBox) source).isSelected()) {
                if (((JCheckBox) source).getText() == "tout") {
                    for (int i = 0; i < j.length; i++) {
                        if (j[i].getText() != "tout") {
                            j[i].setEnabled(false);
                        }
                    }
                    checkArray.add("*");
                } else {
                    for (int i = 0; i < j.length; i++) {
                        if (j[i].getText() == "tout") {
                            j[i].setEnabled(false);
                        }
                    }
                    cpt++;
                    checkArray.add(((JCheckBox) source).getText());
                }
            } else {

                //Si on déselectionne une checkbox
                if (((JCheckBox) source).getText() == "tout") {

                    for (int i = 0; i < j.length; i++) {
                        if (j[i].getText() != "tout") {
                            j[i].setEnabled(true);
                        }
                    }
                }

                for (int i = 0; i < j.length; i++) {
                    if (j[i].getText() != "tout" && j[i].isSelected()) {
                        cpt++;
                    }
                }

                System.out.println(cpt);
                if (cpt == 0) {
                    j[0].setEnabled(true);
                }

                //on parcourt la liste on supprime le nom correspondant
                while (itr.hasNext()) {
                    String name = (String) itr.next();
                    if (name == ((JCheckBox) source).getText() || name == "*") {
                        itr.remove();
                    }
                }
            }

        }
    }

    class BoutonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            field = champ.getText();
            crit = critere.getSelectedItem().toString();
            table = tableBox.getSelectedItem().toString();
            crit2 = critere2.getSelectedItem().toString();
            table2 = tableBox2.getSelectedItem().toString();
            crit3 = critere3.getSelectedItem().toString();
            table3 = tableBox3.getSelectedItem().toString();

            Recherche r1 = new Recherche();

            ResultSetMetaData resultMeta = null;
            ColumnName = null;

            Object source = e.getSource();
            try {

                if (source == valider) {
                    isValider = true;

                    // Si tous les champs sont vides
                    if (crit.equals("") && field.equals("")
                            && table2.equals("") && crit2.equals("")
                            && table3.equals("") && crit3.equals("")) {

                        System.out.println("empty:" + checkArray.isEmpty());
                        int cpt = 0;
                        for (int i = 0; i < j.length; i++) {

                            if (!j[i].isSelected()) {
                                cpt++;
                            }
                        }
                        System.out.println("cpt: " + cpt + "j length: " + j.length);
                        if (cpt == j.length && checkArray.isEmpty()) {
                            // On affiche l'intégralité de la table
                            r = r1.All(connexion, table, " * ");
                        }

                        if (!checkArray.isEmpty()) {
                            colonneSelect = String.join(",", checkArray);
                            r = r1.All(connexion, table, colonneSelect);
                        }

                    } else {
                        
                        // Si les champs recherche avancée ne sont pas remplis
                        if (table2.equals("") && table3.equals("") && crit2.equals("") && crit3.equals("")) {

                            // On indique qu'il n'y a pas de recherche avancée
                            RechercheAvancee = false;
                        } else if (!table2.equals("") || table3.equals("") || !crit2.equals("") || !crit3.equals("")) {

                            RechercheAvancee = true;
                        }

                        if (checkArray.isEmpty()) {
                            r = r1.Requete(connexion, RechercheAvancee, startWith, table, crit, field, table2, table3, crit2, crit3, colonneSelect);
                        }

                        if (!checkArray.isEmpty()) {
                            colonneSelect = String.join(",", checkArray);
                            r = r1.Requete(connexion, RechercheAvancee, startWith, table, crit, field, table2, table3, crit2, crit3, colonneSelect);
                        }

                    }

                    resultMeta = r.getMetaData();
                    ColumnName = new String[resultMeta.getColumnCount() + 1];

                    for (int i = 1; i <= resultMeta.getColumnCount(); i++) {
                        ColumnName[i] = resultMeta.getColumnName(i);
                    }
                    ColumnName[0] = "tout";

                    // APRES CHOIX DE COLONNES, LA REQUETE ENVOIE UNIQUEMENT CELLES SELECTIONNEES
                    if (!checkBoxAdded) {
                        AjouterCheckbox(ColumnName);
                        checkBoxAdded = true;
                    }

                }

                if (source == validerR && isValider == true) {

                    InterfaceResultats f2 = new InterfaceResultats();
                    f2.initTable(r);
                    r = null;
                    isValider = false;

                } else if (source == validerR && isValider != true) {
                    JOptionPane.showMessageDialog(null, "Vous devez valider vos choix avant de lancer la recherche !", "Attention", JOptionPane.WARNING_MESSAGE);
                }
            } catch (SQLException ex) {
                Logger.getLogger(InterfaceRecherche.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }


}
