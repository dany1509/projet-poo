/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vue;

import Modele.*;
import Controler.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;


/**
 *
 * @author Dany
 */
public class OpenPage extends JDialog implements ActionListener{
    
    private String connexion ="";
    private JButton co_locale;
    private JButton co_distante; 
    
    public OpenPage(JFrame parent, String title, boolean modal){
        
        super(parent, title, modal);
        this.addWindowListener(new WindowAdapter() { 
    @Override public void windowClosed(WindowEvent e) { 
      System.exit(0);
    }
  });
        this.setSize(400, 500);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.getContentPane().setBackground(new Color(214, 234, 248));
        this.setLayout(null);
        
        JLabel welcome = new JLabel("Bienvenue sur l'application de gestion de base");
        JLabel suite = new JLabel("de données de Maxime, Julien, et Dany.");
        welcome.setBounds(30, 50, 350, 25);
        suite.setBounds(30,80,300,25);
        Font f1 = welcome.getFont().deriveFont(15.0f); 
        welcome.setFont(f1);
        f1 = suite.getFont().deriveFont(15.0f); 
        suite.setFont(f1);
     
        
        co_locale = new JButton("Se connecter en local");
        co_locale.setBounds(50,150,300,50);
        co_distante = new JButton("Se connecter en distant"); 
        co_distante.setBounds(50,300,300,50);
        f1 = co_locale.getFont().deriveFont(15.0f); 
        co_locale.setFont(f1);
        f1 = co_distante.getFont().deriveFont(15.0f); 
        co_distante.setFont(f1);
        
        co_locale.addActionListener(this);
        co_distante.addActionListener(this);
        this.add(welcome);
        this.add(suite);
        this.add(co_locale);
        this.add(co_distante);
        this.setVisible(true);
        
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==co_locale){
            connexion="colocale";
            //System.out.println(connexion_locale);
            this.setVisible(false);
        }
        if(e.getSource()==co_distante){
            connexion="codistante";
             //System.out.println(connexion_locale);
            this.setVisible(false);
        }
    }
    
    public String getConnexion(){
        return connexion;
    }
    
    
}
