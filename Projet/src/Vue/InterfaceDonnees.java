/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vue;
import Modele.*;
import Controler.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import java.sql.*;
import javax.swing.*;
import java.awt.*;
import java.util.Locale;

import org.jfree.data.*;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;



/**
 *
 * @author Dany
 */
public class InterfaceDonnees extends JFrame {
    private JPanel res; 
    
    public InterfaceDonnees(){
        res = new JPanel();
        this.setTitle("Graphique");
        

        this.setLocationRelativeTo(null);

        this.setContentPane(res);
        //this.setVisible(true);
        this.addWindowListener(new WindowAdapter()
        {
        public void windowClosing(WindowEvent e)
        {
            System.out.println("Window Closing");
        }
        });
        
    }
    
    
    public void InfoChambresLits(ResultSet result){
        try {
            this.setSize(500, 300);
            this.setTitle("Reporting Chambre");
            this.setVisible(true);

            //On récupère les MetaData
            ResultSetMetaData meta = result.getMetaData();

            //Petite manipulation pour obtenir le nombre de lignes
            result.last();
            int rowCount = result.getRow();

            result.beforeFirst();
             int[] numeros = new int [rowCount]; 
             String[] service = new String [rowCount]; 
             String[] nb_lits = new String[rowCount]; 
            int i = 0;

            while(result.next()){
               numeros[i]= result.getInt("no_chambre");
               service[i]= result.getString("code_service");
               nb_lits[i] = result.getString("nb_lits");
               i++; 
        
            }
            //Moyenne de lits par chambre
            float moy_chambre = 0;
            for(int a=0; a<rowCount; a++){

                moy_chambre = moy_chambre + Integer.parseInt(nb_lits[a]);
            }
            moy_chambre=moy_chambre/rowCount; 
            this.setLayout(null);
            JPanel pan = new JPanel();
            pan.setLayout(null);

            pan.setBounds(0, 0, 1500, 900);
            pan.setBackground(new Color(214, 234, 248));
            JLabel moyenne = new JLabel("Moyenne de lits dans chaque chambre : "+moy_chambre);
            moyenne.setBounds(10, 0, 500, 50);


            //Moyenne de lits par service
            float moy_rea = 0;
            float moy_chg = 0;
            float moy_car = 0;
            int nb_chambre_rea =0;
            int nb_chambre_car =0;
            int nb_chambre_chg =0;
            for(int a=0; a<rowCount;a++){
                if(service[a].equals("REA")){
                    moy_rea = moy_rea + Integer.parseInt(nb_lits[a]); 
                    nb_chambre_rea++;
                }
                if(service[a].equals("CHG")){
                    moy_chg = moy_chg + Integer.parseInt(nb_lits[a]); 
                    nb_chambre_chg++;
                }
                if(service[a].equals("CAR")){
                    moy_car = moy_car + Integer.parseInt(nb_lits[a]); 
                    nb_chambre_car++;
                }
            }

            moy_rea = moy_rea/nb_chambre_rea;
            moy_chg = moy_chg/nb_chambre_chg;
            moy_car = moy_car/nb_chambre_car;
            JLabel moyenne_rea = new JLabel("Moyenne de lits dans le service 'REA' : " + moy_rea);
            moyenne_rea.setBounds(10, 50, 500, 50);

             JLabel moyenne_chg = new JLabel("Moyenne de lits dans le service 'CHG' : " + moy_chg);
            moyenne_chg.setBounds(10, 100, 500, 50);

             JLabel moyenne_car = new JLabel("Moyenne de lits dans le service 'CAR' : " + moy_car);
            moyenne_car.setBounds(10,150 , 500, 50);
            
            pan.add(moyenne);
            pan.add(moyenne_rea);
            pan.add(moyenne_chg);
            pan.add(moyenne_car);

            this.add(pan);
            /*
            //Création de l'histogramme
            DefaultCategoryDataset dataset = new DefaultCategoryDataset();
            for(int g=0; g<rowCount; g++){
                   // dataset.addValue(nb_lits[g], "1", numeros[g]);
                    }
            JFreeChart chart = ChartFactory.createBarChart("Nombre de lits par chambre",   					
                                    "Chambre", "Nb de lits", dataset,  PlotOrientation.VERTICAL, false, true, false);
            */
             //On ferme le tout   
            result.close();
    } catch (SQLException e) {
      }	
        
    }
    
    public void InfoTauxOccupation(ResultSet res){
        JPanel result = new JPanel();
        result.setVisible(true);
        this.setSize(500, 300);
        this.setVisible(true);
       try {
      
        //On récupère les MetaData
        ResultSetMetaData meta = res.getMetaData();

        //On initialise un tableau d'Object pour les en-têtes du tableau
        Object[] column = new Object[meta.getColumnCount()];

        for(int i = 1 ; i <= meta.getColumnCount(); i++)
          column[i-1] = meta.getColumnName(i);

        //Petite manipulation pour obtenir le nombre de lignes
        res.last();
        int rowCount = res.getRow();
        Object[][] data = new Object[res.getRow()][meta.getColumnCount()];

        //On revient au départ
        res.beforeFirst();
        int j = 1;

        //On remplit le tableau d'Object[][]
        while(res.next()){
          for(int i = 1 ; i <= meta.getColumnCount(); i++)
            data[j-1][i-1] = res.getObject(i);

          j++;
        }

        //On ferme le tout                                     
        res.close();

        //On enlève le contenu de notre conteneur
        result.removeAll();
        //On y ajoute un JTable
        result.add(new JScrollPane(new JTable(data, column)), BorderLayout.CENTER);
        //On force la mise à jour de l'affichage
        result.revalidate();
        this.add(result);

      } catch (SQLException e) {
        //Dans le cas d'une exception, on affiche une pop-up et on efface le contenu		
        result.removeAll();
        result.add(new JScrollPane(new JTable()), BorderLayout.CENTER);
        result.revalidate();
        JOptionPane.showMessageDialog(null, e.getMessage(), "ERREUR ! ", JOptionPane.ERROR_MESSAGE);
    }	
        
    }
    
    
    public void Docteur(ResultSet res){
        
            this.setSize(500, 300);
            this.setTitle("Reporting Docteur");
            this.setVisible(true);
            JPanel result = new JPanel();
            result.setVisible(true);
            this.setSize(500, 300);
            this.setVisible(true);
           try {

            //On récupère les MetaData
            ResultSetMetaData meta = res.getMetaData();

            //On initialise un tableau d'Object pour les en-têtes du tableau
            Object[] column = new Object[meta.getColumnCount()];

            for(int i = 1 ; i <= meta.getColumnCount(); i++)
              column[i-1] = meta.getColumnName(i);

            //Petite manipulation pour obtenir le nombre de lignes
            res.last();
            int rowCount = res.getRow();
            Object[][] data = new Object[res.getRow()][meta.getColumnCount()];

            //On revient au départ
            res.beforeFirst();
            int j = 1;

            //On remplit le tableau d'Object[][]
            while(res.next()){
              for(int i = 1 ; i <= meta.getColumnCount(); i++)
                data[j-1][i-1] = res.getObject(i);

              j++;
            }

            //On ferme le tout                                     
            res.close();

            //On enlève le contenu de notre conteneur
            result.removeAll();
            //On y ajoute un JTable
            result.add(new JScrollPane(new JTable(data, column)), BorderLayout.CENTER);
            //On force la mise à jour de l'affichage
            result.revalidate();
            this.add(result);

      } catch (SQLException e) {
        //Dans le cas d'une exception, on affiche une pop-up et on efface le contenu		
        result.removeAll();
        result.add(new JScrollPane(new JTable()), BorderLayout.CENTER);
        result.revalidate();
        JOptionPane.showMessageDialog(null, e.getMessage(), "ERREUR ! ", JOptionPane.ERROR_MESSAGE);
    }	

            
        
    }
    
    
    public void salaireInfirmier(ResultSet result){
        try {
            this.setSize(500, 300);
            this.setTitle("Reporting Infirmier");
            this.setVisible(true);

            //On récupère les MetaData
            ResultSetMetaData meta = result.getMetaData();

            //Petite manipulation pour obtenir le nombre de lignes
            result.last();
            int rowCount = result.getRow();

            result.beforeFirst();
             String[] codeservice = new String [rowCount]; 
             int[] salaire = new int [rowCount]; 
            int i = 0;

            while(result.next()){
               salaire[i]= result.getInt("salaire");
               codeservice[i]= result.getString("code_service");
               i++; 
        
            }
            //Moyenne de lits par chambre
            int moy_salaire = 0;
            for(int a=0; a<rowCount; a++){

                moy_salaire = moy_salaire + salaire[a];
            }
            moy_salaire=moy_salaire/rowCount; 
            this.setLayout(null);
            JPanel pan = new JPanel();
            pan.setLayout(null);

            pan.setBounds(0, 0, 1500, 900);
            pan.setBackground(new Color(214, 234, 248));
            JLabel moyenne = new JLabel("Moyenne des salaires des infirmiers dans la totalité de l'hôpital: "+moy_salaire);
            moyenne.setBounds(10, 0, 500, 50);


            //Moyenne de salaire par service
            int moy_rea = 0;
            int moy_chg = 0;
            int moy_car = 0;
            int nb_infirmier_rea =0;
            int nb_infirmier_car =0;
            int nb_infirmier_chg =0;
            for(int a=0; a<rowCount;a++){
                if(codeservice[a].equals("REA")){
                    moy_rea = moy_rea + salaire[a]; 
                    nb_infirmier_rea++;
                }
                if(codeservice[a].equals("CHG")){
                    moy_chg = moy_chg + salaire[a]; 
                    nb_infirmier_chg++;
                }
                if(codeservice[a].equals("CAR")){
                    moy_car = moy_car + salaire[a]; 
                    nb_infirmier_car++;
                }
            }

            moy_rea = moy_rea/nb_infirmier_rea;
            moy_chg = moy_chg/nb_infirmier_chg;
            moy_car = moy_car/nb_infirmier_car;
            JLabel moyenne_rea = new JLabel("Moyenne des salaires dans le service 'REA' : " + moy_rea);
            moyenne_rea.setBounds(10, 50, 500, 50);

             JLabel moyenne_chg = new JLabel("Moyenne des salaires dans le service 'CHG' : " + moy_chg);
            moyenne_chg.setBounds(10, 100, 500, 50);

            JLabel moyenne_car = new JLabel("Moyenne des salaires dans le service 'CAR' : " + moy_car);
            moyenne_car.setBounds(10,150 , 500, 50);
            
            pan.add(moyenne);
            pan.add(moyenne_rea);
            pan.add(moyenne_chg);
            pan.add(moyenne_car);

            this.add(pan);
            /*
            //Création de l'histogramme
            DefaultCategoryDataset dataset = new DefaultCategoryDataset();
            for(int g=0; g<rowCount; g++){
                   // dataset.addValue(nb_lits[g], "1", numeros[g]);
                    }
            JFreeChart chart = ChartFactory.createBarChart("Nombre de lits par chambre",   					
                                    "Chambre", "Nb de lits", dataset,  PlotOrientation.VERTICAL, false, true, false);
            */
             //On ferme le tout   
            result.close();
    } catch (SQLException e) {
      }	
        
    }
    
    public void mutuelle(ResultSet result){
        try {
            this.setSize(500, 500);
            this.setTitle("Reporting Malade");
            this.setVisible(true);

            //On récupère les MetaData
            ResultSetMetaData meta = result.getMetaData();

            //Petite manipulation pour obtenir le nombre de lignes
            result.last();
            int rowCount = result.getRow();

            result.beforeFirst();
            String[] mutuelle = new String [rowCount]; 
             //int[] salaire = new int [rowCount]; 
            int i = 0;

            while(result.next()){
               //salaire[i]= result.getInt("salaire");
               mutuelle[i]= result.getString("mutuelle");
               i++; 
        
            }
                 
            
            
            
            this.setLayout(null);
            JPanel pan = new JPanel();
            pan.setLayout(null);

            pan.setBounds(0, 0, 1500, 900);
            pan.setBackground(new Color(214, 234, 248));
            JLabel nb = new JLabel("Nombre de mutuelle différentes: 12");
            nb.setBounds(10, 0, 500, 25);


            //Moyenne de salaire par service
            int mutuelle_a = 0;
            int mutuelle_b = 0;
            int mutuelle_c = 0;
            int mutuelle_d =0;
            int mutuelle_e =0;
            int mutuelle_f =0;
            int mutuelle_g = 0;
            int mutuelle_h = 0;
            int mutuelle_i = 0;
            int mutuelle_j = 0;
            int mutuelle_k = 0;
            int mutuelle_l = 0;
            
            for(int a=0; a<rowCount;a++){
                if(mutuelle[a].equals("AG2R")){
                    mutuelle_a++; 
                }
                if(mutuelle[a].equals("CCVRP")){
                    mutuelle_b++; 
                }
                if(mutuelle[a].equals("CNAMTS")){
                    mutuelle_c++; 
                }
                if(mutuelle[a].equals("LMDE")){
                    mutuelle_d++; 
                }
                if(mutuelle[a].equals("MAAF")){
                    mutuelle_e++; 
                }
                if(mutuelle[a].equals("MAS")){
                    mutuelle_f++; 
                }
                if(mutuelle[a].equals("MGEN")){
                    mutuelle_g++; 
                }
                if(mutuelle[a].equals("MGSP")){
                    mutuelle_h++; 
                }
                if(mutuelle[a].equals("MMA")){
                    mutuelle_i++; 
                }
                if(mutuelle[a].equals("MNAM")){
                    mutuelle_j++; 
                }
                if(mutuelle[a].equals("MNFTC")){
                    mutuelle_k++; 
                }
                if(mutuelle[a].equals("MNH")){
                    mutuelle_l++; 
                }
                
            }

            int taux_a = (mutuelle_a*100)/rowCount;
            int taux_b = (mutuelle_b*100)/rowCount;
            int taux_c = (mutuelle_c*100)/rowCount;
            int taux_d = (mutuelle_d*100)/rowCount;
            int taux_e = (mutuelle_e*100)/rowCount;
            int taux_f = (mutuelle_f*100)/rowCount;
            int taux_g = (mutuelle_g*100)/rowCount;
            int taux_h = (mutuelle_h*100)/rowCount;
            int taux_i = (mutuelle_i*100)/rowCount;
            int taux_j = (mutuelle_j*100)/rowCount;
            int taux_k = (mutuelle_k*100)/rowCount;
            int taux_l = (mutuelle_l*100)/rowCount;
            
            
            JLabel ag2r = new JLabel("Nombre de malades affiliés à la mutuelle 'AG2R' : " + mutuelle_a + " ("+taux_a+"%)");
            ag2r.setBounds(10, 25, 500, 25);

            JLabel ccvrp = new JLabel("Nombre de malades affiliés à la mutuelle 'CCVRP' : " + mutuelle_b+ " ("+taux_b+"%)");
            ccvrp.setBounds(10, 50, 500, 25);
            JLabel cnamts = new JLabel("Nombre de malades affiliés à la mutuelle 'CNAMTS' : " + mutuelle_c+ " ("+taux_c+"%)");
            cnamts.setBounds(10, 75, 500, 25);
            JLabel lmde = new JLabel("Nombre de malades affiliés à la mutuelle 'LMDE' : " + mutuelle_d+ " ("+taux_d+"%)");
            lmde.setBounds(10, 100, 500, 25);
            JLabel maaf = new JLabel("Nombre de malades affiliés à la mutuelle 'MAAF' : " + mutuelle_e+ " ("+taux_e+"%)");
            maaf.setBounds(10, 125, 500, 25);
            JLabel mas = new JLabel("Nombre de malades affiliés à la mutuelle 'MAS' : " + mutuelle_f+ " ("+taux_f+"%)");
            mas.setBounds(10, 150, 500, 25);
            JLabel mgen = new JLabel("Nombre de malades affiliés à la mutuelle 'MGEN' : " + mutuelle_g+ " ("+taux_g+"%)");
            mgen.setBounds(10, 175, 500, 25);
            JLabel mgsp = new JLabel("Nombre de malades affiliés à la mutuelle 'MGSP' : " + mutuelle_h+ " ("+taux_h+"%)");
            mgsp.setBounds(10, 200, 500, 25);
            JLabel mma = new JLabel("Nombre de malades affiliés à la mutuelle 'MMA' : " + mutuelle_i+ " ("+taux_i+"%)");
            mma.setBounds(10, 225, 500, 25);
            JLabel mnam = new JLabel("Nombre de malades affiliés à la mutuelle 'MNAM' : " + mutuelle_j+ " ("+taux_j+"%)");
            mnam.setBounds(10, 250, 500, 25);
            JLabel mnftc = new JLabel("Nombre de malades affiliés à la mutuelle 'MNFTC' : " + mutuelle_k+ " ("+taux_k+"%)");
            mnftc.setBounds(10, 275, 500, 25);
            JLabel mnh = new JLabel("Nombre de malades affiliés à la mutuelle 'MNH' : " + mutuelle_l+ " ("+taux_l+"%)");
            mnh.setBounds(10, 300, 500, 25);
            
            pan.add(nb);
            pan.add(ag2r);
            pan.add(ccvrp);
            pan.add(cnamts);
            pan.add(lmde);
            pan.add(maaf);
            pan.add(mas);
            pan.add(mgen);
            pan.add(mgsp);
            pan.add(mma);
            pan.add(mnam);
            pan.add(mnftc);
            pan.add(mnh);
            
            

            this.add(pan);
            
             //On ferme le tout   
            result.close();
    } catch (SQLException e) {
      }	
        
    }
    
    public void infirmierNuit(ResultSet res){
        JPanel result = new JPanel();
        result.setVisible(true);
        this.setSize(500, 500);
        this.setVisible(true);
       try {
      
        //On récupère les MetaData
        ResultSetMetaData meta = res.getMetaData();

        //On initialise un tableau d'Object pour les en-têtes du tableau
        Object[] column = new Object[meta.getColumnCount()];

        for(int i = 1 ; i <= meta.getColumnCount(); i++)
          column[i-1] = meta.getColumnName(i);

        //Petite manipulation pour obtenir le nombre de lignes
        res.last();
        int rowCount = res.getRow();
        Object[][] data = new Object[res.getRow()][meta.getColumnCount()];

        //On revient au départ
        res.beforeFirst();
        int j = 1;

        //On remplit le tableau d'Object[][]
        while(res.next()){
          for(int i = 1 ; i <= meta.getColumnCount(); i++)
            data[j-1][i-1] = res.getObject(i);

          j++;
        }

        //On ferme le tout                                     
        res.close();

        //On enlève le contenu de notre conteneur
        result.removeAll();
        //On y ajoute un JTable
        result.add(new JScrollPane(new JTable(data, column)), BorderLayout.CENTER);
        //On force la mise à jour de l'affichage
        result.revalidate();
        this.add(result);

      } catch (SQLException e) {
        //Dans le cas d'une exception, on affiche une pop-up et on efface le contenu		
        result.removeAll();
        result.add(new JScrollPane(new JTable()), BorderLayout.CENTER);
        result.revalidate();
        JOptionPane.showMessageDialog(null, e.getMessage(), "ERREUR ! ", JOptionPane.ERROR_MESSAGE);
    }	
        
    }
    
    public void rapport(ResultSet res){
        JPanel result = new JPanel();
        result.setVisible(true);
        this.setSize(500, 500);
        this.setVisible(true);
       try {
      
        //On récupère les MetaData
        ResultSetMetaData meta = res.getMetaData();

        //On initialise un tableau d'Object pour les en-têtes du tableau
        Object[] column = new Object[meta.getColumnCount()];

        for(int i = 1 ; i <= meta.getColumnCount(); i++)
          column[i-1] = meta.getColumnName(i);

        //Petite manipulation pour obtenir le nombre de lignes
        res.last();
        int rowCount = res.getRow();
        Object[][] data = new Object[res.getRow()][meta.getColumnCount()];

        //On revient au départ
        res.beforeFirst();
        int j = 1;

        //On remplit le tableau d'Object[][]
        while(res.next()){
          for(int i = 1 ; i <= meta.getColumnCount(); i++)
            data[j-1][i-1] = res.getObject(i);

          j++;
        }

        //On ferme le tout                                     
        res.close();

        //On enlève le contenu de notre conteneur
        result.removeAll();
        //On y ajoute un JTable
        result.add(new JScrollPane(new JTable(data, column)), BorderLayout.CENTER);
        //On force la mise à jour de l'affichage
        result.revalidate();
        this.add(result);

      } catch (SQLException e) {
        //Dans le cas d'une exception, on affiche une pop-up et on efface le contenu		
        result.removeAll();
        result.add(new JScrollPane(new JTable()), BorderLayout.CENTER);
        result.revalidate();
        JOptionPane.showMessageDialog(null, e.getMessage(), "ERREUR ! ", JOptionPane.ERROR_MESSAGE);
    }	
        
    }
    
    public void employeD(ResultSet result){
        try {
            this.setSize(500, 300);
            this.setTitle("Reporting Employe");
            this.setVisible(true);

            //On récupère les MetaData
            ResultSetMetaData meta = result.getMetaData();

            //Petite manipulation pour obtenir le nombre de lignes
            result.last();
            int rowCount = result.getRow();

            result.beforeFirst();
            Float[] rapport = new Float [rowCount]; 
            int i = 0;

            while(result.next()){
               rapport[i]= result.getFloat("rapport_d_sur_e");
               i++; 
        
            }
            this.setLayout(null);
            JPanel pan = new JPanel();
            pan.setLayout(null);

            pan.setBounds(0, 0, 1500, 900);
            pan.setBackground(new Color(214, 234, 248));
            JLabel rap = new JLabel("Pourcentage de docteurs dans l'hôpital : " + rapport[1]*100 +"%");
            rap.setBounds(0, 0, 500, 50);

            pan.add(rap);

            this.add(pan); 
            result.close();
    } catch (SQLException e) {
      }	
        
    }
    
    public void employeI(ResultSet result){
        try {
            this.setSize(500, 300);
            this.setTitle("Reporting Employe");
            this.setVisible(true);

            //On récupère les MetaData
            ResultSetMetaData meta = result.getMetaData();

            //Petite manipulation pour obtenir le nombre de lignes
            result.last();
            int rowCount = result.getRow();

            result.beforeFirst();
            Float[] rapport = new Float [rowCount]; 
            int i = 0;

            while(result.next()){
               rapport[i]= result.getFloat("rapport_i_sur_e");
               i++; 
        
            }
            this.setLayout(null);
            JPanel pan = new JPanel();
            pan.setLayout(null);

            pan.setBounds(0, 0, 1500, 900);
            pan.setBackground(new Color(214, 234, 248));
            JLabel rap = new JLabel("Pourcentage d'infirmiers dans l'hôpital : " + rapport[1]*100 +"%");
            rap.setBounds(0, 0, 500, 50);

            pan.add(rap);

            this.add(pan); 
            result.close();                                                                                           
    } catch (SQLException e) {
      }	
        
    }
    
    public void repartitionS(ResultSet result){
        try {
            this.setSize(500, 500);
            this.setTitle("Reporting Docteur");
            this.setVisible(true);

            //On récupère les MetaData
            ResultSetMetaData meta = result.getMetaData();

            //Petite manipulation pour obtenir le nombre de lignes
            result.last();
            int rowCount = result.getRow();

            result.beforeFirst();
            String[] specialite = new String [rowCount]; 
             //int[] salaire = new int [rowCount]; 
            int i = 0;

            while(result.next()){
               //salaire[i]= result.getInt("salaire");
               specialite[i]= result.getString("specialite");
               i++; 
        
            }
                 
            this.setLayout(null);
            JPanel pan = new JPanel();
            pan.setLayout(null);

            pan.setBounds(0, 0, 1500, 900);
            pan.setBackground(new Color(214, 234, 248));
            JLabel nb = new JLabel("Nombre de spécialités différentes: 7");
            nb.setBounds(10, 0, 500, 25);


            //Moyenne de salaire par service
            int spea_a = 0;
            int spea_b = 0;
            int spea_c = 0;
            int spea_d = 0;
            int spea_e = 0;
            int spea_f = 0;
            int spea_g = 0;
            
            
            for(int a=0; a<rowCount;a++){
                if(specialite[a].equals("Anesthesiste")){
                    spea_a++; 
                }
                if(specialite[a].equals("Cardiologue")){
                    spea_b++; 
                }
                if(specialite[a].equals("Generaliste")){
                    spea_c++; 
                }
                if(specialite[a].equals("Orthopediste")){
                    spea_d++; 
                }
                if(specialite[a].equals("Pneumologue")){
                    spea_e++; 
                }
                if(specialite[a].equals("Radiologue")){
                    spea_f++; 
                }
                if(specialite[a].equals("Traumatologue")){
                    spea_g++; 
                }
                
                
            }

            int taux_a = (spea_a*100)/rowCount;
            int taux_b = (spea_b*100)/rowCount;
            int taux_c = (spea_c*100)/rowCount;
            int taux_d = (spea_d*100)/rowCount;
            int taux_e = (spea_e*100)/rowCount;
            int taux_f = (spea_f*100)/rowCount;
            int taux_g = (spea_g*100)/rowCount;
            
            
            JLabel spea = new JLabel("Nombre de docteurs de la spécialité 'Anesthésiste' : " + spea_a + " ("+taux_a+"%)");
            spea.setBounds(10, 25, 500, 25);

            JLabel speab = new JLabel("Nombre de docteurs de la spécialité 'Cardiologue' : " + spea_b+ " ("+taux_b+"%)");
            speab.setBounds(10, 50, 500, 25);
            
            JLabel speac = new JLabel("Nombre de docteurs de la spécialité 'Generaliste' : " + spea_c+ " ("+taux_c+"%)");
            speac.setBounds(10, 75, 500, 25);
            
            JLabel spead = new JLabel("Nombre de docteurs de la spécialité 'Orthopediste' : " + spea_d+ " ("+taux_d+"%)");
            spead.setBounds(10, 100, 500, 25);
            JLabel speae = new JLabel("Nombre de docteurs de la spécialité 'Pneumologue' : " + spea_e+ " ("+taux_e+"%)");
            speae.setBounds(10, 125, 500, 25);
            JLabel speaf = new JLabel("Nombre de docteurs de la spécialité 'Radiologue' : " + spea_f+ " ("+taux_f+"%)");
            speaf.setBounds(10, 150, 500, 25);
            JLabel speag = new JLabel("Nombre de docteurs de la spécialité 'Traumatologue' : " + spea_g+ " ("+taux_g+"%)");
            speag.setBounds(10, 175, 500, 25);
            
            pan.add(nb);
            pan.add(spea);
            pan.add(speab);
            pan.add(speac);
            pan.add(spead);
            pan.add(speae);
            pan.add(speaf);
            pan.add(speag);
            
            

            this.add(pan);
            
             //On ferme le tout   
            result.close();
    } catch (SQLException e) {
      }	
        
    }
    
    public void nbSoigne(ResultSet res){
        
            this.setSize(500, 300);
            this.setTitle("Reporting Docteur");
            this.setVisible(true);
            JPanel result = new JPanel();
            result.setVisible(true);
            this.setSize(500, 300);
            this.setVisible(true);
           try {

            //On récupère les MetaData
            ResultSetMetaData meta = res.getMetaData();

            //On initialise un tableau d'Object pour les en-têtes du tableau
            Object[] column = new Object[meta.getColumnCount()];

            for(int i = 1 ; i <= meta.getColumnCount(); i++)
              column[i-1] = meta.getColumnName(i);

            //Petite manipulation pour obtenir le nombre de lignes
            res.last();
            int rowCount = res.getRow();
            Object[][] data = new Object[res.getRow()][meta.getColumnCount()];

            //On revient au départ
            res.beforeFirst();
            int j = 1;

            //On remplit le tableau d'Object[][]
            while(res.next()){
              for(int i = 1 ; i <= meta.getColumnCount(); i++)
                data[j-1][i-1] = res.getObject(i);

              j++;
            }

            //On ferme le tout                                     
            res.close();

            //On enlève le contenu de notre conteneur
            result.removeAll();
            //On y ajoute un JTable
            result.add(new JScrollPane(new JTable(data, column)), BorderLayout.CENTER);
            //On force la mise à jour de l'affichage
            result.revalidate();
            this.add(result);

      } catch (SQLException e) {
        //Dans le cas d'une exception, on affiche une pop-up et on efface le contenu		
        result.removeAll();
        result.add(new JScrollPane(new JTable()), BorderLayout.CENTER);
        result.revalidate();
        JOptionPane.showMessageDialog(null, e.getMessage(), "ERREUR ! ", JOptionPane.ERROR_MESSAGE);
    }	

            
        
    }
    
    
    public void hospitalisation(ResultSet res){
        
            this.setSize(500, 300);
            this.setTitle("Reporting Docteur");
            this.setVisible(true);
            JPanel result = new JPanel();
            result.setVisible(true);
            this.setSize(500, 300);
            this.setVisible(true);
           try {

            //On récupère les MetaData
            ResultSetMetaData meta = res.getMetaData();

            //On initialise un tableau d'Object pour les en-têtes du tableau
            Object[] column = new Object[meta.getColumnCount()];

            for(int i = 1 ; i <= meta.getColumnCount(); i++)
              column[i-1] = meta.getColumnName(i);

            //Petite manipulation pour obtenir le nombre de lignes
            res.last();
            int rowCount = res.getRow();
            Object[][] data = new Object[res.getRow()][meta.getColumnCount()];

            //On revient au départ
            res.beforeFirst();
            int j = 1;

            //On remplit le tableau d'Object[][]
            while(res.next()){
              for(int i = 1 ; i <= meta.getColumnCount(); i++)
                data[j-1][i-1] = res.getObject(i);

              j++;
            }

            //On ferme le tout                                     
            res.close();

            //On enlève le contenu de notre conteneur
            result.removeAll();
            //On y ajoute un JTable
            result.add(new JScrollPane(new JTable(data, column)), BorderLayout.CENTER);
            //On force la mise à jour de l'affichage
            result.revalidate();
            this.add(result);

      } catch (SQLException e) {
        //Dans le cas d'une exception, on affiche une pop-up et on efface le contenu		
        result.removeAll();
        result.add(new JScrollPane(new JTable()), BorderLayout.CENTER);
        result.revalidate();
        JOptionPane.showMessageDialog(null, e.getMessage(), "ERREUR ! ", JOptionPane.ERROR_MESSAGE);
    }	

            
        
    }
    
}
