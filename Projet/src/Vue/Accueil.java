/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vue;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 *
 * @author Dany
 */
public class Accueil extends JPanel {
    
    public Accueil(){
        
        this.setBackground(new Color(214, 234, 248));
        this.setLayout(null);
        
        JButton image = new JButton();
        image.setBounds(311, 0, 64,64);
        image.setIcon(new ImageIcon("images/hopital.png"));
        
        JLabel label1 = new JLabel("Bienvenue sur l'application de gestion de données de l'hôpital. ");
        JLabel label2 = new JLabel("Le module Recherche vous permet d'effectuer des recherches parmi les dossiers existants. ");
        JLabel label3 = new JLabel("Le module Reporting permet de voir en un clic les chiffres-clés de l'hôpital.");
        JLabel label4 = new JLabel("Enfin, le module Mise A Jour permet de modifier, supprimer ou ajouter des données.");
        label1.setBounds(10, 100, 750, 25);
        Font f1 = label1.getFont().deriveFont(17.0f); 
        label1.setFont(f1);
        label2.setBounds(10, 145, 750, 25);
        f1 = label2.getFont().deriveFont(16.0f); 
        label2.setFont(f1);
        label3.setBounds(10, 190, 750, 25);
        f1 = label3.getFont().deriveFont(16.0f); 
        label3.setFont(f1);
        label4.setBounds(10, 235, 750, 25);
        f1 = label4.getFont().deriveFont(16.0f); 
        label4.setFont(f1);
	
        
        
        this.add(image);
        this.add(label1);
        this.add(label2);
        this.add(label3);
        this.add(label4);
        
      
    }
    
}