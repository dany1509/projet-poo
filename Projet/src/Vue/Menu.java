/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vue;
import Controler.*;
import Modele.*;
import java.sql.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 *
 * @author Dany
 */
public class Menu extends JPanel implements ActionListener {
    
    public String bouton="";
    private Fenetre f ; 
    private Accueil ac; 
    private InterfaceRecherche rc;
    private InterfaceReporting re;
    private InterfaceMaJ mj;
    
    JButton accueil = new JButton("Accueil");
    JButton recherche = new JButton("Recherche");
    JButton reporting = new JButton("Reporting");
    JButton maj = new JButton("MàJ");
    JButton image;
    
    
    public Menu(Fenetre a, Connexion c){
        this.setBackground(new Color(174,214,241));
        this.setMinimumSize(new Dimension(250,800));
        this.setMaximumSize(new Dimension(250,800));
        
        this.setLayout(null);
        
        image = new JButton();
        image.setBounds(95, 0, 64,64);
        image.setIcon(new ImageIcon("images/hopital.png"));
        image.setVisible(false);
        
        this.add(image);
        f = a;
        ac = new Accueil();
        rc = new InterfaceRecherche(c);
        re = new InterfaceReporting(c);
        mj = new InterfaceMaJ(c);
        
        
        accueil.setMaximumSize(new Dimension(100,50));
        Font f1 = accueil.getFont().deriveFont(28.0f); 
        accueil.setFont(f1);
        accueil.setBounds(20,100, 200, 50);
        accueil.addActionListener(this);

        recherche.setPreferredSize(new Dimension(200,50));
        Font f2=recherche.getFont().deriveFont(28.0f); 
        recherche.setFont(f2);
        recherche.setBounds(20,170, 200, 50);
        recherche.addActionListener(this);
        
        reporting.setPreferredSize(new Dimension(200,50));
        Font f3=reporting.getFont().deriveFont(28.0f); 
        reporting.setFont(f3);
        reporting.setBounds(20,240, 200, 50);
        reporting.addActionListener(this);
       
        maj.setPreferredSize(new Dimension(200,50));
        Font f4=recherche.getFont().deriveFont(28.0f); 
        maj.setFont(f4);
        maj.setBounds(20,310, 200, 50);
        maj.addActionListener(this);
        
        
        
        this.add(accueil);
        this.add(recherche);
        this.add(reporting);
        this.add(maj);
    }
    
     public void actionPerformed(ActionEvent e) {   
         
         if(e.getSource()==accueil){
             image.setVisible(false);
             System.out.println("bouton accueil");
             f.updateFenetre(ac);
         }
         if(e.getSource()==recherche){
             image.setVisible(true);
             System.out.println("bouton recherche");
             f.updateFenetre(rc);
             
             
         }
         
         if(e.getSource()==reporting){
             image.setVisible(true);
             System.out.println("bouton reporting");
             f.updateFenetre(re);
                      
         }
         
         if(e.getSource()==maj){
             image.setVisible(true);
             System.out.println("bouton mise à jour");
             f.updateFenetre(mj);
         }

  } 
    
    
    
    
    
    
}